require 'test_helper'

class Comment1sControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment1 = comment1s(:one)
  end

  test "should get index" do
    get comment1s_url
    assert_response :success
  end

  test "should get new" do
    get new_comment1_url
    assert_response :success
  end

  test "should create comment1" do
    assert_difference('Comment1.count') do
      post comment1s_url, params: { comment1: { city_id: @comment1.city_id, comment0_id: @comment1.comment0_id, community_id: @comment1.community_id, content: @comment1.content, district_id: @comment1.district_id, identity_id: @comment1.identity_id, local_id: @comment1.local_id, province_id: @comment1.province_id, published: @comment1.published, score: @comment1.score, user_id: @comment1.user_id } }
    end

    assert_redirected_to comment1_url(Comment1.last)
  end

  test "should show comment1" do
    get comment1_url(@comment1)
    assert_response :success
  end

  test "should get edit" do
    get edit_comment1_url(@comment1)
    assert_response :success
  end

  test "should update comment1" do
    patch comment1_url(@comment1), params: { comment1: { city_id: @comment1.city_id, comment0_id: @comment1.comment0_id, community_id: @comment1.community_id, content: @comment1.content, district_id: @comment1.district_id, identity_id: @comment1.identity_id, local_id: @comment1.local_id, province_id: @comment1.province_id, published: @comment1.published, score: @comment1.score, user_id: @comment1.user_id } }
    assert_redirected_to comment1_url(@comment1)
  end

  test "should destroy comment1" do
    assert_difference('Comment1.count', -1) do
      delete comment1_url(@comment1)
    end

    assert_redirected_to comment1s_url
  end
end
