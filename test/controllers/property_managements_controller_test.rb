require 'test_helper'

class PropertyManagementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @property_management = property_managements(:one)
  end

  test "should get index" do
    get property_managements_url
    assert_response :success
  end

  test "should get new" do
    get new_property_management_url
    assert_response :success
  end

  test "should create property_management" do
    assert_difference('PropertyManagement.count') do
      post property_managements_url, params: { property_management: { email: @property_management.email, identity_id: @property_management.identity_id, name: @property_management.name, note: @property_management.note, pinyin: @property_management.pinyin, socialmedia: @property_management.socialmedia, telephone: @property_management.telephone, user_id: @property_management.user_id, verified: @property_management.verified, website: @property_management.website } }
    end

    assert_redirected_to property_management_url(PropertyManagement.last)
  end

  test "should show property_management" do
    get property_management_url(@property_management)
    assert_response :success
  end

  test "should get edit" do
    get edit_property_management_url(@property_management)
    assert_response :success
  end

  test "should update property_management" do
    patch property_management_url(@property_management), params: { property_management: { email: @property_management.email, identity_id: @property_management.identity_id, name: @property_management.name, note: @property_management.note, pinyin: @property_management.pinyin, socialmedia: @property_management.socialmedia, telephone: @property_management.telephone, user_id: @property_management.user_id, verified: @property_management.verified, website: @property_management.website } }
    assert_redirected_to property_management_url(@property_management)
  end

  test "should destroy property_management" do
    assert_difference('PropertyManagement.count', -1) do
      delete property_management_url(@property_management)
    end

    assert_redirected_to property_managements_url
  end
end
