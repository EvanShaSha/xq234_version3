require 'test_helper'

class ForumComplainsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @forum_complain = forum_complains(:one)
  end

  test "should get index" do
    get forum_complains_url
    assert_response :success
  end

  test "should get new" do
    get new_forum_complain_url
    assert_response :success
  end

  test "should create forum_complain" do
    assert_difference('ForumComplain.count') do
      post forum_complains_url, params: { forum_complain: { approved: @forum_complain.approved, content: @forum_complain.content, forum_id: @forum_complain.forum_id, reason_id: @forum_complain.reason_id, user_id: @forum_complain.user_id } }
    end

    assert_redirected_to forum_complain_url(ForumComplain.last)
  end

  test "should show forum_complain" do
    get forum_complain_url(@forum_complain)
    assert_response :success
  end

  test "should get edit" do
    get edit_forum_complain_url(@forum_complain)
    assert_response :success
  end

  test "should update forum_complain" do
    patch forum_complain_url(@forum_complain), params: { forum_complain: { approved: @forum_complain.approved, content: @forum_complain.content, forum_id: @forum_complain.forum_id, reason_id: @forum_complain.reason_id, user_id: @forum_complain.user_id } }
    assert_redirected_to forum_complain_url(@forum_complain)
  end

  test "should destroy forum_complain" do
    assert_difference('ForumComplain.count', -1) do
      delete forum_complain_url(@forum_complain)
    end

    assert_redirected_to forum_complains_url
  end
end
