require 'test_helper'

class IdentityPhotosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @identity_photo = identity_photos(:one)
  end

  test "should get index" do
    get identity_photos_url
    assert_response :success
  end

  test "should get new" do
    get new_identity_photo_url
    assert_response :success
  end

  test "should create identity_photo" do
    assert_difference('IdentityPhoto.count') do
      post identity_photos_url, params: { identity_photo: { identity_id: @identity_photo.identity_id, name: @identity_photo.name, photo: @identity_photo.photo } }
    end

    assert_redirected_to identity_photo_url(IdentityPhoto.last)
  end

  test "should show identity_photo" do
    get identity_photo_url(@identity_photo)
    assert_response :success
  end

  test "should get edit" do
    get edit_identity_photo_url(@identity_photo)
    assert_response :success
  end

  test "should update identity_photo" do
    patch identity_photo_url(@identity_photo), params: { identity_photo: { identity_id: @identity_photo.identity_id, name: @identity_photo.name, photo: @identity_photo.photo } }
    assert_redirected_to identity_photo_url(@identity_photo)
  end

  test "should destroy identity_photo" do
    assert_difference('IdentityPhoto.count', -1) do
      delete identity_photo_url(@identity_photo)
    end

    assert_redirected_to identity_photos_url
  end
end
