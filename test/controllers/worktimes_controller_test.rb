require 'test_helper'

class WorktimesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worktime = worktimes(:one)
  end

  test "should get index" do
    get worktimes_url
    assert_response :success
  end

  test "should get new" do
    get new_worktime_url
    assert_response :success
  end

  test "should create worktime" do
    assert_difference('Worktime.count') do
      post worktimes_url, params: { worktime: { beginning: @worktime.beginning, city_id: @worktime.city_id, community_id: @worktime.community_id, day: @worktime.day, district_id: @worktime.district_id, duration: @worktime.duration, ending: @worktime.ending, identity_id: @worktime.identity_id, local_id: @worktime.local_id, month: @worktime.month, note: @worktime.note, province_id: @worktime.province_id, suan: @worktime.suan, user_id: @worktime.user_id, year: @worktime.year } }
    end

    assert_redirected_to worktime_url(Worktime.last)
  end

  test "should show worktime" do
    get worktime_url(@worktime)
    assert_response :success
  end

  test "should get edit" do
    get edit_worktime_url(@worktime)
    assert_response :success
  end

  test "should update worktime" do
    patch worktime_url(@worktime), params: { worktime: { beginning: @worktime.beginning, city_id: @worktime.city_id, community_id: @worktime.community_id, day: @worktime.day, district_id: @worktime.district_id, duration: @worktime.duration, ending: @worktime.ending, identity_id: @worktime.identity_id, local_id: @worktime.local_id, month: @worktime.month, note: @worktime.note, province_id: @worktime.province_id, suan: @worktime.suan, user_id: @worktime.user_id, year: @worktime.year } }
    assert_redirected_to worktime_url(@worktime)
  end

  test "should destroy worktime" do
    assert_difference('Worktime.count', -1) do
      delete worktime_url(@worktime)
    end

    assert_redirected_to worktimes_url
  end
end
