require 'test_helper'

class RealEstateDevelopersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @real_estate_developer = real_estate_developers(:one)
  end

  test "should get index" do
    get real_estate_developers_url
    assert_response :success
  end

  test "should get new" do
    get new_real_estate_developer_url
    assert_response :success
  end

  test "should create real_estate_developer" do
    assert_difference('RealEstateDeveloper.count') do
      post real_estate_developers_url, params: { real_estate_developer: { address: @real_estate_developer.address, city_id: @real_estate_developer.city_id, district_id: @real_estate_developer.district_id, email: @real_estate_developer.email, identity_id: @real_estate_developer.identity_id, name: @real_estate_developer.name, note: @real_estate_developer.note, pinyin: @real_estate_developer.pinyin, province_id: @real_estate_developer.province_id, socialmedia: @real_estate_developer.socialmedia, telephone: @real_estate_developer.telephone, user_id: @real_estate_developer.user_id, verified: @real_estate_developer.verified, website: @real_estate_developer.website } }
    end

    assert_redirected_to real_estate_developer_url(RealEstateDeveloper.last)
  end

  test "should show real_estate_developer" do
    get real_estate_developer_url(@real_estate_developer)
    assert_response :success
  end

  test "should get edit" do
    get edit_real_estate_developer_url(@real_estate_developer)
    assert_response :success
  end

  test "should update real_estate_developer" do
    patch real_estate_developer_url(@real_estate_developer), params: { real_estate_developer: { address: @real_estate_developer.address, city_id: @real_estate_developer.city_id, district_id: @real_estate_developer.district_id, email: @real_estate_developer.email, identity_id: @real_estate_developer.identity_id, name: @real_estate_developer.name, note: @real_estate_developer.note, pinyin: @real_estate_developer.pinyin, province_id: @real_estate_developer.province_id, socialmedia: @real_estate_developer.socialmedia, telephone: @real_estate_developer.telephone, user_id: @real_estate_developer.user_id, verified: @real_estate_developer.verified, website: @real_estate_developer.website } }
    assert_redirected_to real_estate_developer_url(@real_estate_developer)
  end

  test "should destroy real_estate_developer" do
    assert_difference('RealEstateDeveloper.count', -1) do
      delete real_estate_developer_url(@real_estate_developer)
    end

    assert_redirected_to real_estate_developers_url
  end
end
