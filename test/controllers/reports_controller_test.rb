require 'test_helper'

class ReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @report = reports(:one)
  end

  test "should get index" do
    get reports_url
    assert_response :success
  end

  test "should get new" do
    get new_report_url
    assert_response :success
  end

  test "should create report" do
    assert_difference('Report.count') do
      post reports_url, params: { report: { attitude: @report.attitude, average: @report.average, building: @report.building, city_id: @report.city_id, closed: @report.closed, community_id: @report.community_id, contact_type: @report.contact_type, district_id: @report.district_id, entity: @report.entity, facility_type: @report.facility_type, fix_date: @report.fix_date, fix_result: @report.fix_result, fix_time: @report.fix_time, floor: @report.floor, identity_id: @report.identity_id, local_id: @report.local_id, note1: @report.note1, note2: @report.note2, province_id: @report.province_id, representative: @report.representative, sex_id: @report.sex_id, surround: @report.surround, unit: @report.unit, user_id: @report.user_id } }
    end

    assert_redirected_to report_url(Report.last)
  end

  test "should show report" do
    get report_url(@report)
    assert_response :success
  end

  test "should get edit" do
    get edit_report_url(@report)
    assert_response :success
  end

  test "should update report" do
    patch report_url(@report), params: { report: { attitude: @report.attitude, average: @report.average, building: @report.building, city_id: @report.city_id, closed: @report.closed, community_id: @report.community_id, contact_type: @report.contact_type, district_id: @report.district_id, entity: @report.entity, facility_type: @report.facility_type, fix_date: @report.fix_date, fix_result: @report.fix_result, fix_time: @report.fix_time, floor: @report.floor, identity_id: @report.identity_id, local_id: @report.local_id, note1: @report.note1, note2: @report.note2, province_id: @report.province_id, representative: @report.representative, sex_id: @report.sex_id, surround: @report.surround, unit: @report.unit, user_id: @report.user_id } }
    assert_redirected_to report_url(@report)
  end

  test "should destroy report" do
    assert_difference('Report.count', -1) do
      delete report_url(@report)
    end

    assert_redirected_to reports_url
  end
end
