require 'test_helper'

class Comment0sControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment0 = comment0s(:one)
  end

  test "should get index" do
    get comment0s_url
    assert_response :success
  end

  test "should get new" do
    get new_comment0_url
    assert_response :success
  end

  test "should create comment0" do
    assert_difference('Comment0.count') do
      post comment0s_url, params: { comment0: { city_id: @comment0.city_id, community_id: @comment0.community_id, content: @comment0.content, district_id: @comment0.district_id, forum_id: @comment0.forum_id, identity_id: @comment0.identity_id, local_id: @comment0.local_id, province_id: @comment0.province_id, published: @comment0.published, score: @comment0.score, user_id: @comment0.user_id } }
    end

    assert_redirected_to comment0_url(Comment0.last)
  end

  test "should show comment0" do
    get comment0_url(@comment0)
    assert_response :success
  end

  test "should get edit" do
    get edit_comment0_url(@comment0)
    assert_response :success
  end

  test "should update comment0" do
    patch comment0_url(@comment0), params: { comment0: { city_id: @comment0.city_id, community_id: @comment0.community_id, content: @comment0.content, district_id: @comment0.district_id, forum_id: @comment0.forum_id, identity_id: @comment0.identity_id, local_id: @comment0.local_id, province_id: @comment0.province_id, published: @comment0.published, score: @comment0.score, user_id: @comment0.user_id } }
    assert_redirected_to comment0_url(@comment0)
  end

  test "should destroy comment0" do
    assert_difference('Comment0.count', -1) do
      delete comment0_url(@comment0)
    end

    assert_redirected_to comment0s_url
  end
end
