require 'test_helper'

class ReportComment0sControllerTest < ActionDispatch::IntegrationTest
  setup do
    @report_comment0 = report_comment0s(:one)
  end

  test "should get index" do
    get report_comment0s_url
    assert_response :success
  end

  test "should get new" do
    get new_report_comment0_url
    assert_response :success
  end

  test "should create report_comment0" do
    assert_difference('ReportComment0.count') do
      post report_comment0s_url, params: { report_comment0: { city_id: @report_comment0.city_id, community_id: @report_comment0.community_id, content: @report_comment0.content, district_id: @report_comment0.district_id, identity_id: @report_comment0.identity_id, local_id: @report_comment0.local_id, province_id: @report_comment0.province_id, published: @report_comment0.published, report_id: @report_comment0.report_id, score: @report_comment0.score, user_id: @report_comment0.user_id } }
    end

    assert_redirected_to report_comment0_url(ReportComment0.last)
  end

  test "should show report_comment0" do
    get report_comment0_url(@report_comment0)
    assert_response :success
  end

  test "should get edit" do
    get edit_report_comment0_url(@report_comment0)
    assert_response :success
  end

  test "should update report_comment0" do
    patch report_comment0_url(@report_comment0), params: { report_comment0: { city_id: @report_comment0.city_id, community_id: @report_comment0.community_id, content: @report_comment0.content, district_id: @report_comment0.district_id, identity_id: @report_comment0.identity_id, local_id: @report_comment0.local_id, province_id: @report_comment0.province_id, published: @report_comment0.published, report_id: @report_comment0.report_id, score: @report_comment0.score, user_id: @report_comment0.user_id } }
    assert_redirected_to report_comment0_url(@report_comment0)
  end

  test "should destroy report_comment0" do
    assert_difference('ReportComment0.count', -1) do
      delete report_comment0_url(@report_comment0)
    end

    assert_redirected_to report_comment0s_url
  end
end
