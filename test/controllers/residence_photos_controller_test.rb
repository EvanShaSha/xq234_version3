require 'test_helper'

class ResidencePhotosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @residence_photo = residence_photos(:one)
  end

  test "should get index" do
    get residence_photos_url
    assert_response :success
  end

  test "should get new" do
    get new_residence_photo_url
    assert_response :success
  end

  test "should create residence_photo" do
    assert_difference('ResidencePhoto.count') do
      post residence_photos_url, params: { residence_photo: { name: @residence_photo.name, photo: @residence_photo.photo, residence_id: @residence_photo.residence_id } }
    end

    assert_redirected_to residence_photo_url(ResidencePhoto.last)
  end

  test "should show residence_photo" do
    get residence_photo_url(@residence_photo)
    assert_response :success
  end

  test "should get edit" do
    get edit_residence_photo_url(@residence_photo)
    assert_response :success
  end

  test "should update residence_photo" do
    patch residence_photo_url(@residence_photo), params: { residence_photo: { name: @residence_photo.name, photo: @residence_photo.photo, residence_id: @residence_photo.residence_id } }
    assert_redirected_to residence_photo_url(@residence_photo)
  end

  test "should destroy residence_photo" do
    assert_difference('ResidencePhoto.count', -1) do
      delete residence_photo_url(@residence_photo)
    end

    assert_redirected_to residence_photos_url
  end
end
