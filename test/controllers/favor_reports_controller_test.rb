require 'test_helper'

class FavorReportsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @favor_report = favor_reports(:one)
  end

  test "should get index" do
    get favor_reports_url
    assert_response :success
  end

  test "should get new" do
    get new_favor_report_url
    assert_response :success
  end

  test "should create favor_report" do
    assert_difference('FavorReport.count') do
      post favor_reports_url, params: { favor_report: { report_id: @favor_report.report_id, score: @favor_report.score, user_id: @favor_report.user_id } }
    end

    assert_redirected_to favor_report_url(FavorReport.last)
  end

  test "should show favor_report" do
    get favor_report_url(@favor_report)
    assert_response :success
  end

  test "should get edit" do
    get edit_favor_report_url(@favor_report)
    assert_response :success
  end

  test "should update favor_report" do
    patch favor_report_url(@favor_report), params: { favor_report: { report_id: @favor_report.report_id, score: @favor_report.score, user_id: @favor_report.user_id } }
    assert_redirected_to favor_report_url(@favor_report)
  end

  test "should destroy favor_report" do
    assert_difference('FavorReport.count', -1) do
      delete favor_report_url(@favor_report)
    end

    assert_redirected_to favor_reports_url
  end
end
