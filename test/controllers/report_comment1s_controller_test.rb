require 'test_helper'

class ReportComment1sControllerTest < ActionDispatch::IntegrationTest
  setup do
    @report_comment1 = report_comment1s(:one)
  end

  test "should get index" do
    get report_comment1s_url
    assert_response :success
  end

  test "should get new" do
    get new_report_comment1_url
    assert_response :success
  end

  test "should create report_comment1" do
    assert_difference('ReportComment1.count') do
      post report_comment1s_url, params: { report_comment1: { city_id: @report_comment1.city_id, community_id: @report_comment1.community_id, content: @report_comment1.content, district_id: @report_comment1.district_id, identity_id: @report_comment1.identity_id, local_id: @report_comment1.local_id, province_id: @report_comment1.province_id, published: @report_comment1.published, report_comment0_id: @report_comment1.report_comment0_id, score: @report_comment1.score, user_id: @report_comment1.user_id } }
    end

    assert_redirected_to report_comment1_url(ReportComment1.last)
  end

  test "should show report_comment1" do
    get report_comment1_url(@report_comment1)
    assert_response :success
  end

  test "should get edit" do
    get edit_report_comment1_url(@report_comment1)
    assert_response :success
  end

  test "should update report_comment1" do
    patch report_comment1_url(@report_comment1), params: { report_comment1: { city_id: @report_comment1.city_id, community_id: @report_comment1.community_id, content: @report_comment1.content, district_id: @report_comment1.district_id, identity_id: @report_comment1.identity_id, local_id: @report_comment1.local_id, province_id: @report_comment1.province_id, published: @report_comment1.published, report_comment0_id: @report_comment1.report_comment0_id, score: @report_comment1.score, user_id: @report_comment1.user_id } }
    assert_redirected_to report_comment1_url(@report_comment1)
  end

  test "should destroy report_comment1" do
    assert_difference('ReportComment1.count', -1) do
      delete report_comment1_url(@report_comment1)
    end

    assert_redirected_to report_comment1s_url
  end
end
