require 'test_helper'

class FavorForumsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @favor_forum = favor_forums(:one)
  end

  test "should get index" do
    get favor_forums_url
    assert_response :success
  end

  test "should get new" do
    get new_favor_forum_url
    assert_response :success
  end

  test "should create favor_forum" do
    assert_difference('FavorForum.count') do
      post favor_forums_url, params: { favor_forum: { forum_id: @favor_forum.forum_id, score: @favor_forum.score, user_id: @favor_forum.user_id } }
    end

    assert_redirected_to favor_forum_url(FavorForum.last)
  end

  test "should show favor_forum" do
    get favor_forum_url(@favor_forum)
    assert_response :success
  end

  test "should get edit" do
    get edit_favor_forum_url(@favor_forum)
    assert_response :success
  end

  test "should update favor_forum" do
    patch favor_forum_url(@favor_forum), params: { favor_forum: { forum_id: @favor_forum.forum_id, score: @favor_forum.score, user_id: @favor_forum.user_id } }
    assert_redirected_to favor_forum_url(@favor_forum)
  end

  test "should destroy favor_forum" do
    assert_difference('FavorForum.count', -1) do
      delete favor_forum_url(@favor_forum)
    end

    assert_redirected_to favor_forums_url
  end
end
