class Residence < ApplicationRecord

  include PublicActivity::Common

  before_save { unit.upcase! }

  has_many :residence_photos, dependent: :destroy

  belongs_to :identity
  belongs_to :community

  has_one :user, through: :identity

  validates :community_id, presence: true
  validates :unit, presence: true, length: { minimum: 2, maximum: 6 }

#it is possible that one unit has two co-owners, several other renters or family members, thus unit cannot be unique.

  validates :area, presence: true, length: { minimum: 2, maximum: 6 }
  validates :role, presence: true
  validates :identity_id, presence: true, uniqueness: true
  #One person can only use one unit property as his primary home.

  # if :role = '住家业主'
  #   validates :unit, uniqueness: { scope: :community_id, message: "一山不容二虎！" }
  # end #the syntex is not working...

#technically speacking, we allow more owners in one unique unit in one community, because we need to find out who has already moved and has not canceled his residence. The old owner is probably not very active anyway.

  default_scope -> { order("RANDOM()") }
#to avoid too many people are approving the same person simultaneously. show ONLY one randomly everytime.

end
