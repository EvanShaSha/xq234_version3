class ReportComment0 < ApplicationRecord
  belongs_to :user
  belongs_to :report
  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province
  belongs_to :identity

  has_many :report_comment1s

  validates :content, presence: true, length: { minimum: 14, maximum: 280 }

  default_scope -> { order(created_at: :desc) }

end
