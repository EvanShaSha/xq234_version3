class RealEstateDeveloper < ApplicationRecord

  belongs_to :province
  belongs_to :city
  belongs_to :district

  belongs_to :user

  has_many :communities

  validates :name, presence: true, length: { minimum: 2, maximum: 20 }
  #validates :telephone, presence: true, length: { maximum: 15 }
  #validates :note, length: { maximum: 140 }

  default_scope -> { order(updated_at: :desc) }
end
