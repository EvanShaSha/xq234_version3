class PropertyManagement < ApplicationRecord

  belongs_to :user

  has_many :communities

  validates :name, presence: true, length: { minimum: 2, maximum: 20 }

  default_scope -> { order(updated_at: :desc) }

end
