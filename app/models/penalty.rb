class Penalty < ApplicationRecord
  belongs_to :penalty_type
  belongs_to :identity #whom receives penalty
  belongs_to :user #who issues penalty

  validates :identity_id,  presence: true
  default_scope -> { order(updated_at: :desc) }
  
end
