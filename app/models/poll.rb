class Poll < ApplicationRecord
  belongs_to :user
  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province

  default_scope -> { order(created_at: :desc) }


  has_many :vote_options, dependent: :destroy

  accepts_nested_attributes_for :vote_options, :reject_if => :all_blank, :allow_destroy => true

  validates :topic, presence: true, length: { minimum: 13, maximum: 140 }

  def normalized_votes_for(option)
    votes_summary == 0 ? 0 : (option.votes.count.to_f / votes_summary) * 100
  end

  def votes_summary
    vote_options.inject(0) {|summary, option| summary + option.votes.count}
  end
end
