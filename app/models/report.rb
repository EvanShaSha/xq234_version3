class Report < ApplicationRecord
  belongs_to :user
  belongs_to :identity

  belongs_to :province
  belongs_to :city
  belongs_to :district
  belongs_to :local
  belongs_to :community

  has_many :favor_reports, dependent: :destroy

  has_many :report_comment0s, dependent: :destroy

  validates :building,  presence: true, length: { maximum: 6 }
  validates :floor,  presence: true, length: { maximum: 6 }
  validates :surround,  presence: true
  validates :unit, length: { maximum: 6 }
  validates :facility_type, length: { maximum: 6 }

  validates :entity,  presence: true
  validates :representative,  presence: true, length: { maximum: 6 }
  validates :sex,  presence: true
  validates :contact_type,  presence: true

  validates :attitude,  presence: true
  validates :fix_time,  presence: true

  validates :note1, presence: true, length: { minimum: 6, maximum: 140 }

  validates :note2, length: { maximum: 140 }

  default_scope -> { order(closed: :asc, created_at: :desc) }

  before_validation do
    self.average = ((attitude.to_i + fix_time.to_i + fix_result.to_i)/3.0).round(2)
  end

end
