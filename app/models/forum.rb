class Forum < ApplicationRecord
  #self.per_page = 2

  belongs_to :user
  belongs_to :identity

  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province

  has_one :forum_complain, dependent: :destroy

  has_many :comment0s, dependent: :destroy
  has_many :favor_forums, dependent: :destroy

  validates :content, presence: true, length: { minimum: 28, maximum: 1400 }

  default_scope -> { order(created_at: :desc) }

  # searchkick word_start: [:content]
  #
  # def search_data
  #   {
  #     content: content,
  #     #published: true
  #     community_id: community_id
  #     #year: year,
  #     #plot: plot
  #   }
  # end

  def more_content
    content.length.to_i - 140
  end

end
