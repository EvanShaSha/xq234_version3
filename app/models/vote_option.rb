class VoteOption < ApplicationRecord
  belongs_to :poll

  has_many :votes, dependent: :destroy
  has_many :users, through: :votes

  validates :title, presence: true
  default_scope -> { order(title: :asc) }
end
