class Local < ApplicationRecord

	belongs_to :district

  has_one :city, through: :district

  has_many :comment0s, dependent: :destroy
	has_many :polls, dependent: :destroy
  has_many :forums, dependent: :destroy
	has_many :reports, dependent: :destroy

  validates :name,  presence: true, length: { maximum: 12 }
	validates :pinyin,  presence: true, length: { minimum: 2, maximum: 12 }
	validates :district_id, presence: true

	#default_scope -> { order(created_at: :desc) }
  default_scope -> { order(pinyin: :asc) }

	#searchkick word_start: [:name]

	def district_name
	  district.try(:name)
	end

	def district_name=(name)
	  self.district = District.find_by(name: name) if name.present?
	end

end
