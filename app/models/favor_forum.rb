class FavorForum < ApplicationRecord
  belongs_to :user
  belongs_to :forum

  validates :user_id, presence: true
  validates :forum_id, presence: true
  validates :user_id, uniqueness: { scope: [:forum_id], message: "已经收藏过了." }
  #message is no show. if forum_id is nil, it won't pull up the recordes
end
