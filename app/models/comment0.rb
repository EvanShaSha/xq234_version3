class Comment0 < ApplicationRecord
  belongs_to :identity
  belongs_to :forum
  belongs_to :user
  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province

  has_many :comment1s, dependent: :destroy

  validates :content, presence: true, length: { minimum: 14, maximum: 280 }
  validates :forum_id, presence: true

  default_scope -> { order(created_at: :desc) }

  after_commit :create_notifications, on: :create

  private

  def create_notifications
    Notification.create do |notification|
      notification.notify_type = 'forum'
      notification.actor = self.user
      notification.user = self.forum.user
      notification.target = self
      notification.second_target = self.forum
      #notification.third_target = self.forum.community
    end
  end
end
