class ForumComplain < ApplicationRecord

  belongs_to :forum
  belongs_to :user
  belongs_to :identity

  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province

  validates :category, presence: true
  validates :content, presence: true, length: { minimum: 4, maximum: 140 }

  default_scope -> { order(legitimate: :desc, updated_at: :desc) }
end
