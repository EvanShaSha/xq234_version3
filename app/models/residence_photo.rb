class ResidencePhoto < ApplicationRecord
  belongs_to :residence

  mount_uploader :photo, ResidencePhotoUploader

  validates :name, presence: true
  validate :photo_size

  default_scope -> { order(created_at: :desc) }

  private

    # Validates the size of an uploaded picture.
    def photo_size
      if photo.size > 1.megabytes
        errors.add(:photo, "照片应该小于 1MB")
      end
    end
    #It does not work on client side...
end
