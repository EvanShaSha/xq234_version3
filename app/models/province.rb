class Province < ActiveRecord::Base
  has_many :cities, dependent: :destroy
  has_many :districts, through: :cities

  has_many :comment0s, dependent: :destroy
	has_many :polls, dependent: :destroy
  has_many :forums, dependent: :destroy
	has_many :reports, dependent: :destroy
  
  def short_name
    @short_name ||= name.sub(/省|市|(回族|壮族|维吾尔)?自治区|特别行政区&/, '')
  end
end
