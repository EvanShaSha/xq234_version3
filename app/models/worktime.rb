class Worktime < ApplicationRecord
  belongs_to :user
  belongs_to :identity
  belongs_to :province
  belongs_to :city
  belongs_to :district
  belongs_to :local
  belongs_to :community

  validates :user_id, uniqueness: { scope: [:year, :month, :day], message: "每人每天只能打卡一次！谢谢您为小区作出的贡献！" }
  #the validation works. But the error message only shows in regular form, not in simple_form. I don't know why. or should I not separate year, month, and day in different column of its database?

  validates :note, presence: true, length: { minimum: 6, maximum: 140 }
  #error messages in Chinese show fine in simple_form. Please check out zh.yml in config folder under locales.

  before_save do
    if ending > beginning
      self.duration = (ending - beginning)/60
      #minutes

      if self.duration > 60
        self.duration = 10 #if more than 60 minutes, count to 0.
      elsif self.duration < 10
        self.duration = 0
      end
      #what if worktime is less than 10 min?
    end
  end

  default_scope -> { order(beginning: :desc) }
end
