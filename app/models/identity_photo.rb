class IdentityPhoto < ApplicationRecord
  belongs_to :identity



  mount_uploader :photo, IdentityPhotoUploader

  # after_save do |record|
  #
  #   full_path = File.join("http://localhost:3000/", "uploads/identity_photo/photo/64/", record.identity_photo[photo])
  #   img = MiniMagick::Image.new(full_path)
  #
  #     img.combine_options do |c|
  #       c.gravity 'SouthWest'
  #       # This is RAILS_ROOT/images/watermark.gif
  #       c.draw "image Over 0,0 0,0 \"http://localhost:3000/uploads/logo.png\""
  #     end
  #
  #     #img.write("http://localhost:3000/uploads/logo2.png")
  #
  #   end

    # after_save do
    #
    #   #full_path = File.join(RAILS_ROOT, record.identity_photo[photo]
    #   img = MiniMagick::Image.new("http://localhost:3000/uploads/identity_photo/photo/64/5c7348fe78f31.jpg")
    #
    #     img.combine_options do |c|
    #       c.gravity 'SouthWest'
    #       # This is RAILS_ROOT/images/watermark.gif
    #       c.draw "image Over 0,0 0,0 \"http://localhost:3000/uploads/logo.png\""
    #     end
    #
    #     #img.write("http://localhost:3000/uploads/logo2.png")
    #
    #   end

  validates :name, presence: true
  validate :photo_size

  default_scope -> { order(created_at: :desc) }

  private

    # Validates the size of an uploaded picture.
    def photo_size
      if photo.size > 1.megabytes
        errors.add(:photo, "照片应该小于1MB")
      end
    end
    #it works on server side, but not on client side.


end
