class FavorReport < ApplicationRecord
  belongs_to :user
  belongs_to :report

  validates :user_id, presence: true
  validates :report_id, presence: true
  validates :user_id, uniqueness: { scope: [:report_id], message: "已经收藏过了." }
  #message is no show when error happens. if forum_id is nil, it won't pull up the recordes


  #default_scope -> { order(created_at: :desc) }
end
