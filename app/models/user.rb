class User < ApplicationRecord

  #acts_as_voter

  # Include default devise modules. Others available are:
  #  :lockable, :timeoutable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_save { email.downcase! }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                 format: { with: VALID_EMAIL_REGEX },
                 uniqueness: { case_sensitive: false }
  #michael hart chapter6

  has_one :identity, dependent: :destroy
  has_one :residence, through: :identity

  has_many :favor_reports
  
  has_many :forums, dependent: :destroy
  has_many :comment0s, dependent: :destroy
  has_many :comment1s, dependent: :destroy

  has_many :polls, dependent: :destroy
  has_many :reports, dependent: :destroy

  has_many :votes, dependent: :destroy
  has_many :vote_options, through: :votes

  #has_many :worktimes, dependent: :destroy
  has_many :penalties, dependent: :destroy
  has_many :favors, dependent: :destroy

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_now


    # if Rails.env.production?
    #   # No worker process in production to handle scheduled tasks
    #   devise_mailer.send(notification, self, *args).deliver_now
    # else
    #   devise_mailer.send(notification, self, *args).deliver_later
    # end
  end

  def voted_for?(poll)
    votes.any? {|v| v.vote_option.poll == poll}
    #vote_options.any? {|v| v.poll == poll }
  end


  default_scope -> { order(email: :asc) }
end
