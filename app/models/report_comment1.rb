class ReportComment1 < ApplicationRecord
  belongs_to :user
  belongs_to :report_comment0
  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province
  belongs_to :identity
end
