class Comment1 < ApplicationRecord
  belongs_to :user
  belongs_to :comment0
  belongs_to :identity

  belongs_to :community
  belongs_to :local
  belongs_to :district
  belongs_to :city
  belongs_to :province

  validates :content, presence: true, length: { minimum: 8, maximum: 140 }
  validates :comment0_id, presence: true

  default_scope -> { order(created_at: :desc) }

  after_commit :create_notifications, on: :create

  private

  def create_notifications
    Notification.create do |notification|
      notification.notify_type = 'comment0'
      notification.actor = self.user
      notification.user = self.comment0.user
      notification.target = self
      notification.second_target = self.comment0
      #notification.third_target = self.forum.community
    end
  end
end
