class Identity < ApplicationRecord

  include PublicActivity::Common
  #tracked owner: ->(controller, model) { controller && controller.current_user }

  belongs_to :user

  has_one :residence, dependent: :destroy
  has_one :penalty

  has_many :forum_complains
  has_many :forums
  has_many :identity_photos, dependent: :destroy
  has_many :comment0s, dependent: :destroy

  validates :lastname, presence: true, length: { maximum: 12 }
  validates :firstname, presence: true, length: { maximum: 12 }
  validates :id_number, presence: true, length: { minimum: 18, maximum: 18 }, uniqueness: true
  #validates :birth_year, presence: true, length: { maximum: 4, minimum: 4 }

  validates :user_id, presence: true, uniqueness: true

  default_scope -> { order("RANDOM()") }
  #to avoid too many people are approving the same person simultaneously

  # if @identity_one
  #   default_scope -> { order("RANDOM()") }
  #
  # else
  #   default_scope -> { order(updated_at: :desc) }
  # end
  # this did not work

  def birth_year
    id_number.to_s.split('')[6..9].join('')
  end

  def age_group
    id_number.to_s.split('')[8]
  end


end
