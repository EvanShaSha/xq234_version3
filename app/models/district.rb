class District < ActiveRecord::Base
  belongs_to :city
  has_one :province, through: :city

  has_many :comment0s, dependent: :destroy
  has_many :polls, dependent: :destroy
  has_many :forums, dependent: :destroy
  has_many :reports, dependent: :destroy
  
  def short_name
    @short_name ||= name.sub(/区|县|市$/,'')
  end

  default_scope -> { order(pinyin: :asc) }
end
