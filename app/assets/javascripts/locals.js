$(document).on('turbolinks:load', function(){
  var locals = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/locals/autocomplete?query=%QUERY',
      wildcard: '%QUERY'
    }
  });
  $('#locals_search').typeahead(null, {
    source: locals
  });

  // return $('#local_district_name').autocomplete({
  //     source: $('#local_district_name').data('autocomplete-source')
  //   });

})


$(document).on('turbolinks:load', function() {
  return $('#local_district_name').autocomplete({
    source: $('#local_district_name').data('autocomplete-source')
  });
});

// jQuery(function() {
//   return $('#local_district_name').autocomplete({
//     //source: ['apple', 'apricot', 'avocado']
//     source: $('#local_district_name').data('autocomplete-source')
//   });
// });
