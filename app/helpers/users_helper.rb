module UsersHelper

  # Returns the Gravatar for the given user.
  def gravatar_for(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", width: "40", height: "40")
  end

  def gravatar_30(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", width: "30", height: "30")
  end

  def gravatar_25(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", width: "25", height: "25")
  end

  def gravatar_female(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", id: "female", width: "30", height: "30")
  end

  def gravatar_female_28(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", id: "female", width: "28", height: "28")
  end

  def gravatar_male(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", id: "male", width: "30", height: "30")
  end

  def gravatar_male_28(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", id: "male", width: "28", height: "28")
  end

  def gravatar_20(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=#{user.logo}"
    image_tag(gravatar_url, alt: user.id, class: "img-circle", width: "20", height: "20")
  end
end
