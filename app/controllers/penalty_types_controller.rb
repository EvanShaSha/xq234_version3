class PenaltyTypesController < ApplicationController
  before_action :set_penalty_type, only: [:show, :edit, :update, :destroy]

  # GET /penalty_types
  # GET /penalty_types.json
  def index
    @penalty_types = PenaltyType.all
  end

  # GET /penalty_types/1
  # GET /penalty_types/1.json
  def show
  end

  # GET /penalty_types/new
  def new
    @penalty_type = PenaltyType.new
  end

  # GET /penalty_types/1/edit
  def edit
  end

  # POST /penalty_types
  # POST /penalty_types.json
  def create
    @penalty_type = PenaltyType.new(penalty_type_params)

    respond_to do |format|
      if @penalty_type.save
        format.html { redirect_to @penalty_type, notice: 'Penalty type was successfully created.' }
        format.json { render :show, status: :created, location: @penalty_type }
      else
        format.html { render :new }
        format.json { render json: @penalty_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /penalty_types/1
  # PATCH/PUT /penalty_types/1.json
  def update
    respond_to do |format|
      if @penalty_type.update(penalty_type_params)
        format.html { redirect_to @penalty_type, notice: 'Penalty type was successfully updated.' }
        format.json { render :show, status: :ok, location: @penalty_type }
      else
        format.html { render :edit }
        format.json { render json: @penalty_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /penalty_types/1
  # DELETE /penalty_types/1.json
  def destroy
    @penalty_type.destroy
    respond_to do |format|
      format.html { redirect_to penalty_types_url, notice: 'Penalty type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_penalty_type
      @penalty_type = PenaltyType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def penalty_type_params
      params.require(:penalty_type).permit(:name, :note)
    end
end
