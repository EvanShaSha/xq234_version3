class ResidencePhotosController < ApplicationController
  before_action :set_residence_photo, only: [:show, :edit, :update, :destroy]

  # GET /residence_photos
  # GET /residence_photos.json
  def index
    @residence_photos = ResidencePhoto.all
  end

  # GET /residence_photos/1
  # GET /residence_photos/1.json
  def show
  end

  # GET /residence_photos/new
  def new
    @residence_photo = ResidencePhoto.new
  end

  # GET /residence_photos/1/edit
  def edit
  end

  # POST /residence_photos
  # POST /residence_photos.json
  def create
    @residence_photo = ResidencePhoto.new(residence_photo_params)

    respond_to do |format|
      if @residence_photo.save
        format.html { redirect_to @residence_photo, notice: '照片上传成功！' }
        format.json { render :show, status: :created, location: @residence_photo }
      else
        format.html { render :new }
        format.json { render json: @residence_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /residence_photos/1
  # PATCH/PUT /residence_photos/1.json
  def update
    respond_to do |format|
      if @residence_photo.update(residence_photo_params)
        format.html { redirect_to @residence_photo, notice: '修改成功！' }
        format.json { render :show, status: :ok, location: @residence_photo }
      else
        format.html { render :edit }
        format.json { render json: @residence_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /residence_photos/1
  # DELETE /residence_photos/1.json
  def destroy
    @residence_photo.destroy
    respond_to do |format|
      format.html { redirect_to show_self_residence_url(@residence_photo.residence.id), notice: '照片删除成功！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_residence_photo
      @residence_photo = ResidencePhoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def residence_photo_params
      params.require(:residence_photo).permit(:residence_id, :name, :photo)
    end
end
