class WorktimesController < ApplicationController
  before_action :set_worktime, only: [:show, :edit, :update, :destroy]

  # GET /worktimes
  # GET /worktimes.json
  def index
    @worktimes = Worktime.where("community_id = ?", current_user.identity.residence.community_id)
    
    @totaltimes = Worktime.where("community_id = ?", current_user.identity.residence.community_id).sum("duration")
  end

  # GET /worktimes/1
  # GET /worktimes/1.json
  def show
  end

  # GET /worktimes/new
  def new
    @worktime = Worktime.new
  end

  # GET /worktimes/1/edit
  def edit
    if current_user.id != @worktime.user_id
      redirect_to @worktime, notice: '您不可以帮别人打卡下班呀！'
    elsif @worktime.suan == true
      redirect_to @worktime, notice: '您已经下班了！'
    end
  end

  def edit_management
    @worktime = Worktime.find(params[:id])
  end

  # POST /worktimes
  # POST /worktimes.json
  def create
    @worktime = Worktime.new(worktime_params)

    respond_to do |format|
      if @worktime.save
        format.html { redirect_to @worktime, notice: '朕，光荣地为小区开始义务工作啦！.' }
        format.json { render :show, status: :created, location: @worktime }
      else
        format.html { render :new }
        format.json { render json: @worktime.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /worktimes/1
  # PATCH/PUT /worktimes/1.json
  def update
    respond_to do |format|
      if @worktime.update(worktime_params)
        format.html { redirect_to @worktime, notice: '工作时间记录成功！' }
        format.json { render :show, status: :ok, location: @worktime }
      else
        format.html { render :edit }
        format.json { render json: @worktime.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /worktimes/1
  # DELETE /worktimes/1.json
  def destroy
    @worktime.destroy
    respond_to do |format|
      format.html { redirect_to worktimes_url, notice: '删除成功！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_worktime
      @worktime = Worktime.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def worktime_params
      params.require(:worktime).permit(:user_id, :beginning, :year, :month, :day, :ending, :duration, :note, :suan, :identity_id, :province_id, :city_id, :district_id, :local_id, :community_id)
    end
end
