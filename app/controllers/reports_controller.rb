class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  # GET /reports
  # GET /reports.json
  def index

    @reports = Report.where("community_id = ?", current_user.identity.residence.community_id).paginate(:page => params[:page], :per_page => 10)

    # if @reports = Report.where("community_id = ? AND closed = ? AND entity = ?", current_user.identity.residence.community_id, true, "物业管理公司").first
    #
    #   @closed_average1 = Report.where("community_id = ? AND closed = ? AND entity = ?", current_user.identity.residence.community_id, true, "物业管理公司").average("average").round(2)
    #
    #   @reports = Report.where("community_id = ?", current_user.identity.residence.community_id)
    #
    # else
    #   @closed_average1 = 0
    #
    #   @reports = Report.where("community_id = ?", current_user.identity.residence.community_id)
    #
    # end

    #@closed_average2 = Report.where("community_id = ? AND closed = ? AND entity = ?", current_user.identity.residence.community_id, true, "房地产开发商").average("average").round(2)
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params)

    respond_to do |format|
      if @report.save
        format.html { redirect_to @report, notice: '成功创建新汇报！' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to @report, notice: '成功鉴定最终结果！' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: '成功删除一个汇报！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:user_id, :building, :surround, :floor, :facility_type, :unit, :entity, :representative, :sex, :contact_type, :fix_date, :attitude, :fix_time, :fix_result, :average, :note1, :identity_id, :closed, :note2, :province_id, :city_id, :district_id, :local_id, :community_id)
    end
end
