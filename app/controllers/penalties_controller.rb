class PenaltiesController < ApplicationController
  before_action :set_penalty, only: [:show, :edit, :update, :destroy]

  # GET /penalties
  # GET /penalties.json
  def index
    @penalties = Penalty.all
  end

  # GET /penalties/1
  # GET /penalties/1.json
  def show
  end

  # GET /penalties/new
  def new
    if current_user.level > 49
      @penalty = Penalty.new
      @identity = Identity.find(params[:id])
    else
      redirect_to root_url, notice: '权限不够！'  
    end
  end

  # GET /penalties/1/edit
  def edit
  end

  # POST /penalties
  # POST /penalties.json
  def create
    @penalty = Penalty.new(penalty_params)

    respond_to do |format|
      if @penalty.save
        format.html { redirect_to @penalty, notice: 'Penalty was successfully created.' }
        format.json { render :show, status: :created, location: @penalty }
      else
        format.html { render :new }
        format.json { render json: @penalty.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /penalties/1
  # PATCH/PUT /penalties/1.json
  def update
    respond_to do |format|
      if @penalty.update(penalty_params)
        format.html { redirect_to @penalty, notice: 'Penalty was successfully updated.' }
        format.json { render :show, status: :ok, location: @penalty }
      else
        format.html { render :edit }
        format.json { render json: @penalty.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /penalties/1
  # DELETE /penalties/1.json
  def destroy
    @penalty.destroy
    respond_to do |format|
      #format.html { redirect_to penalties_community_url(@penalty.community_id), notice: '成功删除惩罚！.' }

      format.html { redirect_to penalties_url, notice: '成功删除惩罚！.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_penalty
      @penalty = Penalty.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def penalty_params
      params.require(:penalty).permit(:penalty_type_id, :identity_id, :starting_date, :ending_date, :note, :user_id, :community_id, :local_id, :district_id, :city_id, :province_id)
    end
end
