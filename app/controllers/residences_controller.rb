class ResidencesController < ApplicationController
  before_action :set_residence, only: [:show, :edit, :update, :destroy]

  # GET /residences
  # GET /residences.json
  def index
    @num_residence_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.read").size
    @num_residence_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.update").size

    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'

    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    elsif @num_residence_read > 2
      redirect_to root_url, notice: '您审阅她他人信息的次数超过限额，谢谢！'
    elsif @num_residence_update > 2
      redirect_to root_url, notice: '您验证她他人信息的次数超过限额，谢谢！'
    else

      @residences = Residence.where("community_id = ? AND status = ? AND ready = ?", current_user.identity.residence.community_id, '等待验证', true)
      @residence_waiting = Residence.where("community_id = ? AND status = ? AND ready = ?", current_user.identity.residence.community_id, '等待验证', true).limit(1)
      #@total_users_community = Residence.where("community_id = #{@@num} AND status = '已经验证'").size
      @total_users_community = Residence.where("community_id = ? AND status = ?", current_user.identity.residence.community_id, '已经验证').size
      #What if someone has waited for so long, says 30 days, and nobody wants to approve him? We do not know how many users are willing to approve others. This is for future project. We can manually check database anyway, says once every 30 days.
      #will add '.limit(1)' in production and cover the name.
    end
  end
  # GET /residences/1
  # GET /residences/1.json
  def show_self
    @residence = Residence.find(params[:id])

    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写您的个人身份信息，谢谢！'

    elsif current_user.identity.residence.nil?
      redirect_to root_url, notice: '请填写您的居住状况，谢谢！'

    elsif @residence.identity.user.id == current_user.id
      @residencephotos = ResidencePhoto.where("residence_id = ?", @residence.id)
    else
      redirect_to root_url, notice: '请注意保护她他人隐私，谢谢！'
    end

  end

  def show
    @num_residence_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.read").size
    @num_residence_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.update").size
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况。谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    elsif @residence.status != '等待验证'
      redirect_to root_url, notice: '此用户已经完成验证过程，谢谢！'
    elsif @num_residence_read > 92
      redirect_to root_url, notice: '您审阅她他人信息的次数超过限额，谢谢！'
    elsif @num_residence_update > 92
      redirect_to root_url, notice: '您验证她他人信息的次数超过限额，谢谢！'
    else
      @residence = Residence.find(params[:id])
      @residencephotos = ResidencePhoto.where("residence_id = ?", @residence.id)
      @residence.create_activity :read, owner: current_user, recipient: @residence.identity.user
      #With Chrome browser, two exactly the same read entry will get inserted into database. It is a browser issue. No fix yet.

    end
  end

  # GET /residences/new
  def new
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'

    elsif current_user.identity.residence
      redirect_to root_url, notice: '您已经填写了居住状况。谢谢！'
    else
      @residence = Residence.new
    end
  end

  def edit_self
    #if @identity.id == current_user.identity.id
      @residence = Residence.find(params[:id])
    #end
  end

  # GET /residences/1/edit
  def edit
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    elsif @residence.status != '等待验证'
      redirect_to root_url, notice: '此用户已经完成验证过程，谢谢！'
    else
      @residence = Residence.find(params[:id])
    end
  end

  # POST /residences
  # POST /residences.json
  def create
    @residence = Residence.new(residence_params)

    respond_to do |format|
      if @residence.save
        format.html { redirect_to show_self_residence_path(@residence.id), notice: '您的居住状况填写成功！' }
        format.json { render :show, status: :created, location: @residence }
      else
        format.html { render :new }
        format.json { render json: @residence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /residences/1
  # PATCH/PUT /residences/1.json
  def update

    respond_to do |format|

      if @residence.update(residence_params)
        @residence.create_activity :update, owner: current_user, recipient: @residence.identity.user
        format.html { redirect_to root_url, notice: '操作成功！' }
        format.json { render :show, status: :ok, location: @residence }
      else
        format.html { render :edit }
        format.json { render json: @residence.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /residences/1
  # DELETE /residences/1.json
  def destroy
    @residence.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: '您的居住状况删除成功！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_residence
      @residence = Residence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def residence_params
      params.require(:residence).permit(:unit, :area, :identity_id, :community_id, :role, :status, :ready, :property_id)
    end
end
