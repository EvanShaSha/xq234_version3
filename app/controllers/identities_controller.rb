class IdentitiesController < ApplicationController

  before_action :set_identity, only: [:show, :edit, :update, :destroy]
  # GET /identities
  # GET /identities.json
  def index
    @num_identity_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.read").size
    @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size
    @waitingidentity = Identity.where("status = ? AND ready = ?", '等待验证', true ).size

    if current_user.email == '38@xq234.com'
      #38@xq234.com is admin
      @identities = Identity.where(status: '等待验证').limit(20)

      #38@xq234.com has admin access
    elsif current_user.identity.nil?
      redirect_to root_url, notice: '您必须要填写个人身份信息，谢谢！'

    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '您的个人身份信息必须是“已经验证”，谢谢！'

    elsif @num_identity_read > 2 && current_user.level < 50
      redirect_to root_url, notice: '您审阅她他人信息的次数超过限额，谢谢！'

    elsif @num_identity_update > 2 && current_user.level < 50
      redirect_to root_url, notice: '您验证她他人信息的次数超过限额，谢谢！'
    else
      #@identities = Identity.where("status = ?", '等待验证')
      @identities = Identity.where("status = ? AND ready = ?", '等待验证', true )
      @identity_one = Identity.where("status = ? AND ready = ?", '等待验证', true ).limit(1)
      #What if someone has waited for so long, says 30 days, and nobody wants to approve him? We do not know how many users are willing to approve others. This is for future project. We can manually check database anyway, says once every 7 days.
      #will add '.limit(1)' in production and cover the name.
    end
  end

  # GET /identities/1
  # GET /identities/1.json
  def show_self
     @identity = Identity.find(params[:id])
    # @identityphotos = IdentityPhoto.where("identity_id = ?", @identity.id)

    #@num_identity_readon = PublicActivity::Activity.where(recipient_id: @identity, key: "identity.read").size

    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写您的个人身份信息，谢谢！'

    elsif current_user.id == @identity.user.id
      #@identity = Identity.find(params[:id])
      @identityphotos = IdentityPhoto.where("identity_id = ?", @identity.id)
    else
      redirect_to root_url, notice: '请注意保护她他人隐私，谢谢！'
      #@num_identity_destroy = PublicActivity::Activity.where(owner_id: current_user, key: "identity.destroy").size
    end
  end

  def show

    @num_identity_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.read").size
    @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size

    if current_user.email == '38@xq234.com'
      @identity = Identity.find(params[:id])
      #admin can have unlimited reads. do not record the reads.

      # if current_user.email == '38@xq234.com' && @num_identity_read < 3
      #   @identity = Identity.find(params[:id])
      #   @identity.create_activity :read, owner: current_user, recipient: @identity.user
      #   volunteer in a community can have 3 reads.

    elsif current_user.identity.nil?
      redirect_to root_url, notice: '您必须要填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '您的个人身份信息必须是“已经验证”，谢谢！'

    elsif @identity.status != '等待验证'
      redirect_to root_url, notice: '此用户已经完成验证过程，谢谢！'
    elsif @num_identity_read > 82
      redirect_to root_url, notice: '您已经完成了审阅任务，谢谢！'
    elsif @num_identity_update > 2
      redirect_to root_url, notice: '您已经完成了验证任务，谢谢！'
    else
      @identity = Identity.find(params[:id])
      @identity.create_activity :read, owner: current_user, recipient: @identity.user

      @identityphotos = IdentityPhoto.where("identity_id = ?", @identity.id)

    end
  end

  # GET /identities/new
  def new
    @num_identity_destroy = PublicActivity::Activity.where(owner_id: current_user, key: "identity.destroy").size
    #a user can not create and delete too many his identity.

    if @num_identity_destroy > 2
      redirect_to root_url, notice: '您填写的个人身份信息次数已经３次了。请电邮support@xq234.com寻求帮助, 谢谢！'

    elsif current_user.identity
       redirect_to root_url, notice: '您已经填写了个人身份信息，谢谢！'

     else
       @identity = Identity.new
     end
  end

  def edit_self
    #if @identity.id == current_user.identity.id
      @identity = Identity.find(params[:id])
    #end

  end
  # GET /identities/1/edit
  def edit

    @num_identity_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.read").size

    @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size

    if current_user.email == '38@xq234.com'
      @identity = Identity.find(params[:id])
      #'38@xq234.com' is an admin who has the unlimited editing numbers.
    elsif current_user.level > 49
      @identity = Identity.find(params[:id])

    elsif current_user.identity.nil?
      redirect_to root_url, notice: '您必须要填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '您的个人身份信息必须是“已经验证”，谢谢！'
    elsif @identity.status != '等待验证'
      redirect_to root_url, notice: '此用户已经完成验证过程，谢谢！'
    # elsif current_user.id = @identity.user.id
    #   redirect_to root_url, notice: '您不能自己验证自己。谢谢！'
    # goes to '您必须是“已经验证”。谢谢！' since he is not approved.
    elsif @num_identity_read > 2
      redirect_to root_url, notice: '您审阅她他人信息的次数超过限额，谢谢！'
      #read first before editing, so reading number should be 1 more than editing number.
      #if one read 3 times without editing, he has lost the editing privilage.
    elsif @num_identity_update > 2
      redirect_to root_url, notice: '您验证她他人信息的次数超过限额，谢谢！'
    else
      @identity = Identity.find(params[:id])

      @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size
    end
  end

  # POST /identities
  # POST /identities.json
  def create

      @identity = Identity.new(identity_params)
      respond_to do |format|
        if @identity.save
          format.html { redirect_to show_self_identity_path(@identity.id), notice: '您的身份信息填写成功！' }
          format.json { render :show, status: :created, location: @identity }
        else
          format.html { render :new }
          format.json { render json: @identity.errors, status: :unprocessable_entity }
        end
      end
  end

  # PATCH/PUT /identities/1
  # PATCH/PUT /identities/1.json
  def update
    if @identity.status != '等待验证'
      redirect_to root_url, notice: '此用户已经完成验证过程，谢谢！'
    else
      respond_to do |format|
        if @identity.update(identity_params)

          @identity.create_activity :update, owner: current_user, recipient: @identity.user

          format.html { redirect_to root_url, notice: '操作成功！'}
          format.json { render :show, status: :ok, location: @identity }
        else
          format.html { render :edit }
          format.json { render json: @identity.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /identities/1
  # DELETE /identities/1.json
  def destroy

    @identity.create_activity :destroy, owner: current_user, recipient: @identity.user

    @identity.destroy

    respond_to do |format|
      format.html { redirect_to root_url, notice: '您的身份信息删除成功！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_identity
      @identity = Identity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def identity_params
      params.require(:identity).permit(:lastname, :firstname, :user_id, :sex, :id_number, :status, :blacklisted, :ready)
    end

end
