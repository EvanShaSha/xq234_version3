class FavorReportsController < ApplicationController
  before_action :set_favor_report, only: [:show, :edit, :update, :destroy]

  # GET /favor_reports
  # GET /favor_reports.json
  def index
    #@favor_reports = FavorReport.all
    @favor_reports = FavorReport.paginate(:page => params[:page], :per_page => 10).where("user_id = ?", current_user.id)
    #paginate will not work with limit(100)

  end

  # GET /favor_reports/1
  # GET /favor_reports/1.json
  def show
  end

  # GET /favor_reports/new
  def new
    @favor_report = FavorReport.new
  end

  # GET /favor_reports/1/edit
  def edit
  end

  # POST /favor_reports
  # POST /favor_reports.json
  def create
    @favor_report = FavorReport.new(favor_report_params)

    respond_to do |format|
      if @favor_report.save
        format.html { redirect_to report_path(@favor_report.report.id), notice: '收藏成功.' }
        format.json { render :show, status: :created, location: @favor_report }
      else
        format.html { render :new }
        format.json { render json: @favor_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /favor_reports/1
  # PATCH/PUT /favor_reports/1.json
  def update
    respond_to do |format|
      if @favor_report.update(favor_report_params)
        format.html { redirect_to @favor_report, notice: 'Favor report was successfully updated.' }
        format.json { render :show, status: :ok, location: @favor_report }
      else
        format.html { render :edit }
        format.json { render json: @favor_report.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favor_reports/1
  # DELETE /favor_reports/1.json
  def destroy
    @favor_report.destroy
    respond_to do |format|
      format.html { redirect_to favor_reports_path, notice: '删除收藏成功.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favor_report
      @favor_report = FavorReport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favor_report_params
      params.require(:favor_report).permit(:user_id, :report_id, :score)
    end
end
