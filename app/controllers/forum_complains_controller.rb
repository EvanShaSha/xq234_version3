class ForumComplainsController < ApplicationController
  before_action :set_forum_complain, only: [:show, :edit, :update, :destroy]

  # GET /forum_complains
  # GET /forum_complains.json
  def index
    @forum_complains = ForumComplain.all
    #@forum_complains = ForumComplain.where("community_id = ?", current_user.identity.residence.community_id)
  end

  # GET /forum_complains/1
  # GET /forum_complains/1.json
  def show
    @forum_complain = ForumComplain.find(params[:id])
    @penalties = Penalty.where("identity_id = ?", @forum_complain.forum.user.identity.id)
  end

  # GET /forum_complains/new
  def new
    @forum_complain = ForumComplain.new
    #@forum = Forum.where(published: true).find(params[:id])
  end

  # GET /forum_complains/1/edit
  def edit
    if @forum_complain.forum.user.id == current_user.id or @forum_complain.user.id == current_user.id
      redirect_to @forum_complain, notice: '只有除被举报人和举报人之外的第三人，才能够让举报成立．'
    elsif current_user.level > 49
      @forum_complain = ForumComplain.find(params[:id])
    end
  end

  # POST /forum_complains
  # POST /forum_complains.json
  def create
    @forum_complain = ForumComplain.new(forum_complain_params)

    respond_to do |format|
      if @forum_complain.save
        format.html { redirect_to @forum_complain, notice: 'Forum complain was successfully created.' }
        format.json { render :show, status: :created, location: @forum_complain }
      else
        format.html { render :new }
        format.json { render json: @forum_complain.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum_complains/1
  # PATCH/PUT /forum_complains/1.json
  def update
    respond_to do |format|
      if @forum_complain.update(forum_complain_params)
        format.html { redirect_to @forum_complain, notice: 'Forum complain was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_complain }
      else
        format.html { render :edit }
        format.json { render json: @forum_complain.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum_complains/1
  # DELETE /forum_complains/1.json
  def destroy
    @forum_complain.destroy
    respond_to do |format|
      format.html { redirect_to forum_complains_community_url(@forum_complain.community_id), notice: 'Forum complain was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum_complain
      @forum_complain = ForumComplain.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_complain_params
      params.require(:forum_complain).permit(:content, :forum_id, :user_id, :category, :community_id, :local_id, :district_id, :city_id, :province_id, :legitimate, :identity_id)
    end
end
