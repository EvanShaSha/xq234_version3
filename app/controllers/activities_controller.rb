class ActivitiesController < ApplicationController

  def index
    if current_user.identity.nil?
      redirect_to root_url, notice: '您必须要填写个人身份信息。谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '您的个人身份信息必须是“已经验证”。谢谢！'
    else
  	  @activities = PublicActivity::Activity.order("created_at desc")
      
   end
  end
end
