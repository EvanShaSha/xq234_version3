class ReportComment1sController < ApplicationController
  before_action :set_report_comment1, only: [:show, :edit, :update, :destroy]

  # GET /report_comment1s
  # GET /report_comment1s.json
  def index
    @report_comment1s = ReportComment1.where("published = ? AND report_comment0_id = ?", true, params[:id]).paginate(:page => params[:page], :per_page => 1)
  end

  # GET /report_comment1s/1
  # GET /report_comment1s/1.json
  def show
    @report_comment1 = ReportComment1.where("published = ?", true).find(params[:id])
  end

  # GET /report_comment1s/new
  def new
    @report_comment1 = ReportComment1.new
  end

  # GET /report_comment1s/1/edit
  def edit
  end

  # POST /report_comment1s
  # POST /report_comment1s.json
  def create
    @report_comment1 = ReportComment1.new(report_comment1_params)

    respond_to do |format|
      if @report_comment1.save
        format.html { redirect_to report_comment0_path(@report_comment1.report_comment0.id), notice: '成功添加回复.' }
        format.json { render :show, status: :created, location: @report_comment1 }
      else
        format.html { render :new }
        format.json { render json: @report_comment1.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /report_comment1s/1
  # PATCH/PUT /report_comment1s/1.json
  def update
    respond_to do |format|
      if @report_comment1.update(report_comment1_params)
        format.html { redirect_to @report_comment1, notice: 'Report comment1 was successfully updated.' }
        format.json { render :show, status: :ok, location: @report_comment1 }
      else
        format.html { render :edit }
        format.json { render json: @report_comment1.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /report_comment1s/1
  # DELETE /report_comment1s/1.json
  def destroy
    @report_comment1.destroy
    respond_to do |format|
      format.html { redirect_to report_comment1s_url, notice: 'Report comment1 was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report_comment1
      @report_comment1 = ReportComment1.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_comment1_params
      params.require(:report_comment1).permit(:user_id, :report_comment0_id, :content, :published, :score, :community_id, :local_id, :district_id, :city_id, :province_id, :identity_id)
    end
end
