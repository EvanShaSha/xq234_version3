class PollsController < ApplicationController

  def index

    if current_user.identity.nil? or current_user.identity.status != '已经验证' or current_user.identity.residence.nil? or current_user.identity.residence.status != '已经验证'
      redirect_to root_url, notice: '您的身份信息和居住状况都必须“已经验证”。谢谢！'
    else
      #@community = Community.find(params[:community_id])
      #community = Community.find(params[:community_id])
      #@polls = community.polls.where(published: true).order('created_at DESC')
      @polls = Poll.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true)

    end

  end

  def show

    if current_user.identity.nil? or current_user.identity.status != '已经验证' or current_user.identity.residence.nil? or current_user.identity.residence.status != '已经验证'
      redirect_to root_url, notice: '您的身份信息和居住状况都必须“已经验证”。谢谢！'
    else
      @poll = Poll.where(published: true).includes(:vote_options).find_by_id(params[:id])
      @vote = Vote.new
    end
  end


  def new

    if current_user.identity.nil? or current_user.identity.status != '已经验证' or current_user.identity.residence.nil? or current_user.identity.residence.status != '已经验证'
      redirect_to root_url, notice: '您的身份信息和居住状况都必须“已经验证”。谢谢！'
    else
      #community = Community.find(params[:community_id])
      #@poll = community.polls.build

      @poll = Poll.new
    end
  end

  def edit
    community = Community.find(params[:community_id])
    @poll = community.polls.find_by_id(params[:id])
  end

  def update
    @poll = Poll.find_by_id(params[:id])
    if @poll.update_attributes(poll_params)
      flash[:success] = 'Poll was updated!'
      redirect_to poll_path
    else
      render 'edit'
    end
  end

  def create

    @poll = Poll.new(poll_params)
    if @poll.save
      #flash[:success] = 'Poll was created!'
      #redirect_to community_polls_path
      #redirect_to community_polls_community_path(@poll.community_id)
      redirect_to @poll, notice: '创建成功！'
    else
      render 'new'
    end
  end

  def destroy
    # community = Community.find(params[:community_id])
    # @poll = community.polls.find_by_id(params[:id])
    @poll = Poll.find_by_id(params[:id])
    #if @poll.user.id == current_user.id && (Time.zone.now - @poll.created_at)/60 < 11180
      #it can be deleted within 180 minutes and by author herself only
      @poll.destroy
        #flash[:success] = 'Poll was destroyed!'
        redirect_to polls_path, notice: '删除成功！'
        #redirect_to polls_community_path(@poll.community_id), notice: '删除成功！'

    #else
      #redirect_to @poll, notice: '很抱歉！已经超过了可删除的时限！'
    #end

  end

  private

  def poll_params
    params.require(:poll).permit(:topic, :deadline, :user_id, :community_id, :local_id, :district_id, :city_id, :province_id, :published, vote_options_attributes: [:id, :title, :_destroy])
  end

end
