class FavorForumsController < ApplicationController
  before_action :set_favor_forum, only: [:show, :edit, :update, :destroy]

  # GET /favor_forums
  # GET /favor_forums.json
  def index
    @favor_forums = FavorForum.paginate(:page => params[:page], :per_page => 10).where("user_id = ?", current_user.id)
  end

  # GET /favor_forums/1
  # GET /favor_forums/1.json
  def show
  end

  # GET /favor_forums/new
  def new
    @favor_forum = FavorForum.new
  end

  # GET /favor_forums/1/edit
  def edit
  end

  # POST /favor_forums
  # POST /favor_forums.json
  def create
    @favor_forum = FavorForum.new(favor_forum_params)

    respond_to do |format|
      if @favor_forum.save
        format.html { redirect_to forum_path(@favor_forum.forum.id), notice: '收藏成功.' }
        format.json { render :show, status: :created, location: @favor_forum }
      else
        format.html { render :new }
        format.json { render json: @favor_forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /favor_forums/1
  # PATCH/PUT /favor_forums/1.json
  def update
    respond_to do |format|
      if @favor_forum.update(favor_forum_params)
        format.html { redirect_to @favor_forum, notice: 'Favor forum was successfully updated.' }
        format.json { render :show, status: :ok, location: @favor_forum }
      else
        format.html { render :edit }
        format.json { render json: @favor_forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favor_forums/1
  # DELETE /favor_forums/1.json
  def destroy
    @favor_forum.destroy
    respond_to do |format|
      format.html { redirect_to favor_forums_url, notice: '删除成功.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favor_forum
      @favor_forum = FavorForum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favor_forum_params
      params.require(:favor_forum).permit(:user_id, :forum_id, :score)
    end
end
