class IdentityPhotosController < ApplicationController
  before_action :set_identity_photo, only: [:show, :edit, :update, :destroy]

  # GET /identity_photos
  # GET /identity_photos.json
  def index
    @identity_photos = IdentityPhoto.all
  end

  # GET /identity_photos/1
  # GET /identity_photos/1.json
  def show
  end

  # GET /identity_photos/new
  def new
    @identity_photo = IdentityPhoto.new
  end

  # GET /identity_photos/1/edit
  def edit
  end

  # POST /identity_photos
  # POST /identity_photos.json
  def create
    @identity_photo = IdentityPhoto.new(identity_photo_params)

    respond_to do |format|
      if @identity_photo.save
        format.html { redirect_to @identity_photo, notice: '照片上传成功！' }
        format.json { render :show, status: :created, location: @identity_photo }
      else
        format.html { render :new }
        format.json { render json: @identity_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /identity_photos/1
  # PATCH/PUT /identity_photos/1.json
  def update
    respond_to do |format|
      if @identity_photo.update(identity_photo_params)
        format.html { redirect_to @identity_photo, notice: '修改成功！' }
        format.json { render :show, status: :ok, location: @identity_photo }
      else
        format.html { render :edit }
        format.json { render json: @identity_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /identity_photos/1
  # DELETE /identity_photos/1.json
  def destroy
    @identity_photo.destroy
    respond_to do |format|
      format.html { redirect_to show_self_identity_url(@identity_photo.identity.id), notice: '照片删除成功！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_identity_photo
      @identity_photo = IdentityPhoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def identity_photo_params
      params.require(:identity_photo).permit(:identity_id, :name, :photo)
    end
end
