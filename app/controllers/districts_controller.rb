class DistrictsController < ApplicationController
  def index
     @districts = District.order(:name).where("name like ?", "%#{params[:term]}%")
     render json: @districts.map(&:name)
   end
 end
