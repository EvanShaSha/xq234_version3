class PropertyManagementsController < ApplicationController
  before_action :set_property_management, only: [:show, :edit, :update, :destroy]

  # GET /property_managements
  # GET /property_managements.json
  def index
    @property_managements = PropertyManagement.all
  end

  # GET /property_managements/1
  # GET /property_managements/1.json
  def show
  end

  # GET /property_managements/new
  def new
    @property_management = PropertyManagement.new
  end

  # GET /property_managements/1/edit
  def edit
  end

  # POST /property_managements
  # POST /property_managements.json
  def create
    @property_management = PropertyManagement.new(property_management_params)

    respond_to do |format|
      if @property_management.save
        format.html { redirect_to @property_management, notice: '成功创建新的物业管理公司！' }
        format.json { render :show, status: :created, location: @property_management }
      else
        format.html { render :new }
        format.json { render json: @property_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /property_managements/1
  # PATCH/PUT /property_managements/1.json
  def update
    respond_to do |format|
      if @property_management.update(property_management_params)
        format.html { redirect_to @property_management, notice: '成功更新物业管理公司！' }
        format.json { render :show, status: :ok, location: @property_management }
      else
        format.html { render :edit }
        format.json { render json: @property_management.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /property_managements/1
  # DELETE /property_managements/1.json
  def destroy
    @property_management.destroy
    respond_to do |format|
      format.html { redirect_to property_managements_url, notice: '成功删除物业管理公司！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_property_management
      @property_management = PropertyManagement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def property_management_params
      params.require(:property_management).permit(:name, :telephone, :note, :pinyin, :website, :email, :socialmedia, :user_id, :verified)
    end
end
