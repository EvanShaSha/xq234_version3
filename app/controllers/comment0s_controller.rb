class Comment0sController < ApplicationController
  before_action :set_comment0, only: [:show, :edit, :update, :destroy]

  # GET /comment0s
  # GET /comment0s.json
  def index
    @comment0s = Comment0.where("published = ? AND forum_id = ?", true, params[:id]).paginate(:page => params[:page], :per_page => 1)
  end

  # GET /comment0s/1
  # GET /comment0s/1.json
  def show
    @comment0 = Comment0.where("published = ?", true).find(params[:id])

  end

  # GET /comment0s/new
  def new
    @comment0 = Comment0.new
  end

  # GET /comment0s/1/edit
  def edit
  end

  # POST /comment0s
  # POST /comment0s.json
  def create
    @comment0 = Comment0.new(comment0_params)

    respond_to do |format|
      if @comment0.save
        format.html { redirect_to forum_path(@comment0.forum.id), notice: '成功添加评论.' }
        format.json { render :show, status: :created, location: @comment0 }
      else
        format.html { render :new }
        format.json { render json: @comment0.error, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comment0s/1
  # PATCH/PUT /comment0s/1.json
  def update
    respond_to do |format|
      if @comment0.update(comment0_params)
        format.html { redirect_to forum_path(@comment0.forum.id), notice: '成功禁止发布.' }
        format.json { render :show, status: :ok, location: @comment0 }
      else
        format.html { render :edit }
        format.json { render json: @comment0.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comment0s/1
  # DELETE /comment0s/1.json
  def destroy
    @comment0.destroy
    respond_to do |format|
      format.html { redirect_to forum_path(@comment0.forum.id), notice: '成功删除.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment0
      @comment0 = Comment0.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment0_params
      params.require(:comment0).permit(:identity_id, :forum_id, :content, :published, :user_id, :score, :community_id, :local_id, :district_id, :city_id, :province_id)
    end
end
