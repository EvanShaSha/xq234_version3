class ApplicationController < ActionController::Base
  #before_filter :configure_permitted_parameters, if: :devise_controller?

  protect_from_forgery with: :exception
  before_filter :authenticate_user!

  include PublicActivity::StoreController

  # protected
  #   def configure_permitted_parameters
  #   #devise_parameter_sanitizer.for(:sign_in) {|u| u.permit(:signin)}
  #   devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:email, :emailmd5, :password, :password_confirmation)}
  # end

  
end
