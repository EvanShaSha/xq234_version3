class RealEstateDevelopersController < ApplicationController
  before_action :set_real_estate_developer, only: [:show, :edit, :update, :destroy]

  # GET /real_estate_developers
  # GET /real_estate_developers.json
  def index
    @real_estate_developers = RealEstateDeveloper.first(10)
  end

  # GET /real_estate_developers/1
  # GET /real_estate_developers/1.json
  def show
  end

  # GET /real_estate_developers/new
  def new
    @real_estate_developer = RealEstateDeveloper.new
  end

  # GET /real_estate_developers/1/edit
  def edit
  end

  # POST /real_estate_developers
  # POST /real_estate_developers.json
  def create
    @real_estate_developer = RealEstateDeveloper.new(real_estate_developer_params)

    respond_to do |format|
      if @real_estate_developer.save
        format.html { redirect_to @real_estate_developer, notice: '成功创建房地产开发公司!' }
        format.json { render :show, status: :created, location: @real_estate_developer }
      else
        format.html { render :new }
        format.json { render json: @real_estate_developer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /real_estate_developers/1
  # PATCH/PUT /real_estate_developers/1.json
  def update
    respond_to do |format|
      if @real_estate_developer.update(real_estate_developer_params)
        format.html { redirect_to @real_estate_developer, notice: '成功更新房地产开发公司!' }
        format.json { render :show, status: :ok, location: @real_estate_developer }
      else
        format.html { render :edit }
        format.json { render json: @real_estate_developer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /real_estate_developers/1
  # DELETE /real_estate_developers/1.json
  def destroy
    @real_estate_developer.destroy
    respond_to do |format|
      format.html { redirect_to real_estate_developers_url, notice: '成功删除房地产开发公司!' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_real_estate_developer
      @real_estate_developer = RealEstateDeveloper.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def real_estate_developer_params
      params.require(:real_estate_developer).permit(:name, :pinyin, :telephone, :note, :province_id, :city_id, :district_id, :address, :website, :email, :socialmedia, :user_id, :verified)
    end
end
