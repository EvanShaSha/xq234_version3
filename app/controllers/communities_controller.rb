class CommunitiesController < ApplicationController
  before_action :set_community, only: [:show, :edit, :update, :destroy]
  # GET /communities
  # GET /communities.json
  def index
    @communities = Community.all

  end

  def forums
    @forums = Forum.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true)
  end

  def forum_complains
    @forum_complains = ForumComplain.where("community_id = ?", current_user.identity.residence.community_id)
  end

  def polls
    @polls = Poll.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true)
  end

  def penalties
    @penalties = Penalty.where("community_id = ?", current_user.identity.residence.community_id)
  end

  # GET /communities/1
  # GET /communities/1.json
  def show
    if current_user.identity.nil? or current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '您的身份信息必须“已经验证”。谢谢！'

      # @@num = current_user.residence.community_id
      # @total_users_community = Residence.where("community_id = #{@@num} AND status_id = 3").size
    else

    end
  end

  # GET /communities/new
  def new
    @community = Community.new
    if current_user.identity.nil? or current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请填写个人身份信息，再等到个人身份信息“已经验证”。谢谢！'
    else

    end
    #will only have admin to access it.
  end

  # GET /communities/1/edit
  def edit
  end

  def edit_volunteer
    @community = Community.find(params[:id])
  end
  # POST /communities
  # POST /communities.json
  def create
    @community = Community.new(community_params)

    respond_to do |format|
      if @community.save
        format.html { redirect_to @community, notice: '成功提交申请开通小区！' }
        format.json { render :show, status: :created, location: @community }
      else
        format.html { render :new }
        format.json { render json: @community.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /communities/1
  # PATCH/PUT /communities/1.json
  def update
    respond_to do |format|
      if @community.update(community_params)
        format.html { redirect_to @community, notice: '成功更新小区！' }
        format.json { render :show, status: :ok, location: @community }
      else
        format.html { render :edit }
        format.json { render json: @community.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /communities/1
  # DELETE /communities/1.json
  def destroy
    @community.destroy
    respond_to do |format|
      format.html { redirect_to communities_url, notice: '成功删除小区！' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_community
      @community = Community.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def community_params
      params.require(:community).permit!
    end
end
