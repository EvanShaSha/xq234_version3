class HomeController < ApplicationController

	#before_filter :authenticate_user!

  def index


    if current_user.identity.nil?
    #1
    else
    #2
      if current_user.identity.status == '已经验证'
      #A
        
        @waitingidentity = Identity.where("status = ? AND ready = ?", '等待验证', true ).size
        @num_identity_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.read").size
        @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size

        if current_user.identity.residence.nil?
        #I
          # @waitingidentity = Identity.where("status = ? AND created_at < ?", '等待验证', Time.zone.now - 3600).size
          # @num_identity_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.read").size
          # @num_identity_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "identity.update").size

        elsif current_user.identity.residence.status == '已经验证'
        #II

          @waitingresidence = Residence.where("community_id = ? AND status = ? AND created_at < ? AND role = ?", current_user.identity.residence.community_id, '等待验证', Time.zone.now - 3600, '住家业主').size
          @num_residence_read = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.read").size
          @num_residence_update = PublicActivity::Activity.where(owner_id: current_user.id, key: "residence.update").size

        end
        #III
      else
      #B

      end
      #D
    end
    #3

  end
end
# order is very important.
