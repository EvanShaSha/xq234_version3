class ForumsController < ApplicationController
  #before_action :set_forum, only: [:show, :edit, :update, :destroy, :upvote]
  before_action :set_forum, only: [:show, :edit, :update, :destroy]
  # GET /forums
  # GET /forums.json
  def index
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    else

      # search = params[:term].present? ? params[:term] : nil
      #
      # @forums = if search
      #   Forum.search(search)
      #   #Forum.search(search, match: :phrase, where: { community_id: 23 })
      #   #Forum.search(search, fields: [:content, :community_id], where: { community_id: 23 })
      #   #Forum.search(search, page: params[:page], per_page: 7, match: :phrase)
      # else
        @forums = Forum.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true).paginate(:page => params[:page], :per_page => 10)
      end
    #end
  end
  # GET /forums/1
  # GET /forums/1.json
  def show
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    elsif @forum.community_id != current_user.identity.residence.community_id
      redirect_to root_url, notice: '这一贴不在此小区！'
    elsif @forum.published != true
      redirect_to root_url, notice: '这一贴已经被撤销！'
    else

      @forum = Forum.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true).find(params[:id])
      #@cs = Forum.comment0s.where("forum_id = ?", params[:id])
      #@cs = Forum.comment0s.find(params[:id])
      #@cs = Comment0.find(params[:id])
      #@cs = Comment0.where("forum_id = ?", params[:id])
    end
  end

  # GET /forums/new
  def new
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后......谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后......谢谢！'
    else
      @forum = Forum.new
    end

  end

  # GET /forums/1/edit
  def edit
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请在您的个人身份得到“已经验证”以后．谢谢！'
    elsif current_user.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢！'
    elsif current_user.residence.status != '已经验证'
      redirect_to root_url, notice: '请在您的居住状态得到“已经验证”以后．谢谢！'
    #elsif @forum.community_id != current_user.identity.residence.community_id
      #redirect_to root_url, notice: '这一贴不在此小区！'

      # so that admin live in a different xiaoqu can edit.
    elsif @forum.published != true
      redirect_to root_url, notice: '这一贴已经被撤销！'
    elsif current_user.level < 50
      redirect_to root_url, notice: '权限不够！'
    else
      @forum = Forum.where("published = ?", true).find(params[:id])
      #@forum = Forum.where("community_id = ? AND published = ?", current_user.identity.residence.community_id, true).find(params[:id])
      #@forum = Forum.find(params[:id])
    end
  end

  # def upvote
  #   @forum.upvote_from current_user
  #   redirect_to forums_path
  # end

  # POST /forums
  # POST /forums.json
  def create
    @forum = Forum.new(forum_params)
    #@forum.save
    respond_to do |format|
      if @forum.save
        format.html { redirect_to @forum, notice: '成功发布！' }
        format.json { render :show, status: :created, location: @forum }
      else
        format.html { render :new }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forums/1
  # PATCH/PUT /forums/1.json
  def update
    respond_to do |format|
      if @forum.update(forum_params)
        format.html { redirect_to forum_complain_path(@forum.forum_complain.id), notice: '成功撤销帖子！' }
        #format.html { redirect_to forum_complains_community_path(@forum.community_id), notice: '更改成功！' }
        #format.json { render :show, status: :ok, location: @forum }
      else
        format.html { render :edit }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forums/1
  # DELETE /forums/1.json
  def destroy

    if @forum.user.id == current_user.id && (Time.zone.now - @forum.created_at)/60 < 15
      @forum.destroy
      respond_to do |format|
        format.html { redirect_to forums_path, notice: '成功删除！' }
        #format.html { redirect_to forums_community_path(@forum.community_id), notice: '成功删除！' }
        format.json { head :no_content }
      end
    else
      redirect_to forums_path, notice: '已经超过15分钟，无法删除．'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum
      @forum = Forum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_params
      params.require(:forum).permit(:content, :user_id, :community_id, :local_id, :district_id, :city_id, :province_id, :published, :identity_id, :score)
    end

end
