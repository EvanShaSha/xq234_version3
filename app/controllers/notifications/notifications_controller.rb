module Notifications
  class NotificationsController < Notifications::ApplicationController
    def index
      @notifications = notifications.includes(:actor).order('id desc').page(params[:page])
      @notifications = notifications.includes(:actor).order('id desc').paginate(:page => params[:page], :per_page => 10)
      unread_ids = @notifications.reject(&:read?).select(&:id)
      Notification.read!(unread_ids)

      @notification_groups = @notifications.group_by { |note| note.created_at.to_date }
    end

    def clean
      notifications.delete_all
      redirect_to notifications_path
    end

    private

    def notifications
      raise '您必须登入．' unless current_user
      Notification.where(user_id: current_user.id)
    end
  end
end
