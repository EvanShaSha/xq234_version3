class ReportComment0sController < ApplicationController
  before_action :set_report_comment0, only: [:show, :edit, :update, :destroy]

  # GET /report_comment0s
  # GET /report_comment0s.json
  def index
    #@report_comment0s = ReportComment0.all
    @report_comment0s = ReportComment0.where("published = ? AND report_id = ?", true, params[:id]).paginate(:page => params[:page], :per_page => 1)
  end

  # GET /report_comment0s/1
  # GET /report_comment0s/1.json
  def show
  end

  # GET /report_comment0s/new
  def new
    @report_comment0 = ReportComment0.new
  end

  # GET /report_comment0s/1/edit
  def edit
  end

  # POST /report_comment0s
  # POST /report_comment0s.json
  def create
    @report_comment0 = ReportComment0.new(report_comment0_params)

    respond_to do |format|
      if @report_comment0.save
        format.html { redirect_to report_path(@report_comment0.report.id), notice: '成功添加评论.' }
        format.json { render :show, status: :created, location: @report_comment0 }
      else
        format.html { render :new }
        format.json { render json: @report_comment0.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /report_comment0s/1
  # PATCH/PUT /report_comment0s/1.json
  def update
    respond_to do |format|
      if @report_comment0.update(report_comment0_params)
        format.html { redirect_to @report_comment0, notice: 'Report comment0 was successfully updated.' }
        format.json { render :show, status: :ok, location: @report_comment0 }
      else
        format.html { render :edit }
        format.json { render json: @report_comment0.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /report_comment0s/1
  # DELETE /report_comment0s/1.json
  def destroy
    @report_comment0.destroy
    respond_to do |format|
      format.html { redirect_to report_comment0s_url, notice: 'Report comment0 was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_report_comment0
      @report_comment0 = ReportComment0.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_comment0_params
      params.require(:report_comment0).permit(:user_id, :report_id, :content, :published, :score, :community_id, :local_id, :district_id, :city_id, :province_id, :identity_id)
    end
end
