class VotesController < ApplicationController

  def create
    if current_user && params[:poll] && params[:poll][:id] && params[:vote_option] && params[:vote_option][:id]
      @poll = Poll.find_by_id(params[:poll][:id])
      @option = @poll.vote_options.find_by_id(params[:vote_option][:id])
      if @option && @poll && !current_user.voted_for?(@poll)
        current_user.votes.create({vote_option_id: @option.id})
      else
        render js: 'alert(\'您的这次投票不能被记录下来. 您是否已经投过票了？\');'
      end
    else
      render js: 'alert(\'您的这次投票不能被记录下来.\');'
    end
  end

end
