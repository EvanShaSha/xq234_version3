class UsersController < ApplicationController

  before_action :set_user, only: [:show, :edit, :update] # probably want to keep using this

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息。谢谢！'
    end
  end

  # def new
  #   @user = User.new
  # end


  # # GET /users/1
  # # GET /users/1.json
  def show
    #@user = User.includes(:vote_options).find_by_id(params[:id])
    if current_user.identity.nil?
      redirect_to root_url, notice: '请填写个人身份信息，谢谢！'
    elsif current_user.identity.status != '已经验证'
      redirect_to root_url, notice: '请等待身份被验证了之后，谢谢!'
    elsif current_user.identity.residence.nil?
      redirect_to root_url, notice: '请填写居住状况，谢谢!'
    elsif current_user.identity.residence.status != '已经验证'
      redirect_to root_url, notice: '请居住状况被验证了之后，谢谢!'
    # else
    #   @totalminutes = Worktime.where("user_id = ?", (params[:id])).sum("duration")
    else
      #@user = User.find(params[:id])
      @total_comment0_scores = Comment0.where("user_id = ?", (params[:id])).sum("score")
    end
  end

  # GET /users/1/edit
  def edit

  end

  # def create
  #   @user = User.new(user_params)
  #
  #   respond_to do |format|
  #     if @user.save
  #       format.html { redirect_to @real_estate_developer, notice: '成功创建房地产开发商!' }
  #       format.json { render :show, status: :created, location: @real_estate_developer }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @real_estate_developer.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /users/1
  # # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: '用户信息被成功更新.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit!
    end

end
