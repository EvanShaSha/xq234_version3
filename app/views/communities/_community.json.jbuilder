json.extract! community, :id, :name, :pinyin, :local_id, :address, :total_units, :populations, :starting_year, :created_at, :updated_at
json.url community_url(community, format: :json)