json.extract! penalty_type, :id, :name, :note, :created_at, :updated_at
json.url penalty_type_url(penalty_type, format: :json)