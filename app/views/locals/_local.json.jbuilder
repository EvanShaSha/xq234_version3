json.extract! local, :id, :name, :pinyin, :district_id, :created_at, :updated_at
json.url local_url(local, format: :json)