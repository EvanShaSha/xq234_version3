json.extract! identity_photo, :id, :identity_id, :name, :photo, :created_at, :updated_at
json.url identity_photo_url(identity_photo, format: :json)