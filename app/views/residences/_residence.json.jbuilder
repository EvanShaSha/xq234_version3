json.extract! residence, :id, :unit, :area, :identity_id, :community_id, :role_id, :status_id, :created_at, :updated_at
json.url residence_url(residence, format: :json)