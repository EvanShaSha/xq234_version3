json.extract! favor_report, :id, :user_id, :report_id, :score, :created_at, :updated_at
json.url favor_report_url(favor_report, format: :json)