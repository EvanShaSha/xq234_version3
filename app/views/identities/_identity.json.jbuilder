json.extract! identity, :id, :lastname, :firstname, :user_id, :sex_id, :id_type_id, :id_number, :birth_year, :status_id, :created_at, :updated_at
json.url identity_url(identity, format: :json)