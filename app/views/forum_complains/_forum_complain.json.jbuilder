json.extract! forum_complain, :id, :content, :forum_id, :user_id, :reason_id, :approved, :created_at, :updated_at
json.url forum_complain_url(forum_complain, format: :json)