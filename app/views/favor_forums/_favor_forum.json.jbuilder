json.extract! favor_forum, :id, :user_id, :forum_id, :score, :created_at, :updated_at
json.url favor_forum_url(favor_forum, format: :json)