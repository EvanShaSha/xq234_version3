json.extract! forum, :id, :title, :content, :user_id, :community_id, :local_id, :district_id, :city_id, :province_id, :published, :created_at, :updated_at
json.url forum_url(forum, format: :json)