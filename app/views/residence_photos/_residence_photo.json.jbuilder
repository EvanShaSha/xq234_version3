json.extract! residence_photo, :id, :residence_id, :name, :photo, :created_at, :updated_at
json.url residence_photo_url(residence_photo, format: :json)