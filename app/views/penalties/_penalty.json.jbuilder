json.extract! penalty, :id, :penalty_type_id, :identity_id, :starting_date, :ending_date, :note, :user_id, :created_at, :updated_at
json.url penalty_url(penalty, format: :json)