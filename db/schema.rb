# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190824112734) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "trackable_type"
    t.integer  "trackable_id"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.string   "key"
    t.text     "parameters"
    t.string   "recipient_type"
    t.integer  "recipient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
    t.index ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
    t.index ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "province_id"
    t.integer  "level"
    t.string   "zip_code"
    t.string   "pinyin"
    t.string   "pinyin_abbr"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["level"], name: "index_cities_on_level", using: :btree
    t.index ["name"], name: "index_cities_on_name", using: :btree
    t.index ["pinyin"], name: "index_cities_on_pinyin", using: :btree
    t.index ["pinyin_abbr"], name: "index_cities_on_pinyin_abbr", using: :btree
    t.index ["province_id"], name: "index_cities_on_province_id", using: :btree
    t.index ["zip_code"], name: "index_cities_on_zip_code", using: :btree
  end

  create_table "comment0s", force: :cascade do |t|
    t.integer  "identity_id"
    t.integer  "forum_id"
    t.text     "content"
    t.boolean  "published"
    t.integer  "user_id"
    t.integer  "score"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_comment0s_on_city_id", using: :btree
    t.index ["community_id"], name: "index_comment0s_on_community_id", using: :btree
    t.index ["district_id"], name: "index_comment0s_on_district_id", using: :btree
    t.index ["forum_id"], name: "index_comment0s_on_forum_id", using: :btree
    t.index ["identity_id"], name: "index_comment0s_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_comment0s_on_local_id", using: :btree
    t.index ["province_id"], name: "index_comment0s_on_province_id", using: :btree
    t.index ["user_id"], name: "index_comment0s_on_user_id", using: :btree
  end

  create_table "comment1s", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "comment0_id"
    t.text     "content"
    t.boolean  "published"
    t.integer  "identity_id"
    t.integer  "score"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_comment1s_on_city_id", using: :btree
    t.index ["comment0_id"], name: "index_comment1s_on_comment0_id", using: :btree
    t.index ["community_id"], name: "index_comment1s_on_community_id", using: :btree
    t.index ["district_id"], name: "index_comment1s_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_comment1s_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_comment1s_on_local_id", using: :btree
    t.index ["province_id"], name: "index_comment1s_on_province_id", using: :btree
    t.index ["user_id"], name: "index_comment1s_on_user_id", using: :btree
  end

  create_table "communities", force: :cascade do |t|
    t.string   "name"
    t.string   "pinyin"
    t.integer  "local_id"
    t.string   "address"
    t.integer  "starting_year"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "user_id"
    t.string   "category",                 default: "商品住房"
    t.integer  "building"
    t.integer  "floor"
    t.integer  "unit_per_floor"
    t.integer  "occupation_rate"
    t.integer  "total_household"
    t.text     "introduction"
    t.integer  "real_estate_developer_id", default: 1
    t.integer  "property_management_id",   default: 1
    t.boolean  "launched",                 default: false
    t.text     "note"
    t.integer  "businessfloors",           default: 0
    t.decimal  "managementfee",            default: "0.0"
    t.integer  "undergroundparkingfloors", default: 0
    t.boolean  "openparking",              default: false
    t.boolean  "ownerassociation",         default: false
    t.index ["local_id"], name: "index_communities_on_local_id", using: :btree
    t.index ["property_management_id"], name: "index_communities_on_property_management_id", using: :btree
    t.index ["real_estate_developer_id"], name: "index_communities_on_real_estate_developer_id", using: :btree
    t.index ["user_id"], name: "index_communities_on_user_id", using: :btree
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name"
    t.integer  "city_id"
    t.string   "pinyin"
    t.string   "pinyin_abbr"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["city_id"], name: "index_districts_on_city_id", using: :btree
    t.index ["name"], name: "index_districts_on_name", using: :btree
    t.index ["pinyin"], name: "index_districts_on_pinyin", using: :btree
    t.index ["pinyin_abbr"], name: "index_districts_on_pinyin_abbr", using: :btree
  end

  create_table "favor_forums", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "forum_id"
    t.integer  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["forum_id"], name: "index_favor_forums_on_forum_id", using: :btree
    t.index ["user_id"], name: "index_favor_forums_on_user_id", using: :btree
  end

  create_table "favor_reports", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "report_id"
    t.integer  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["report_id"], name: "index_favor_reports_on_report_id", using: :btree
    t.index ["user_id"], name: "index_favor_reports_on_user_id", using: :btree
  end

  create_table "forum_complains", force: :cascade do |t|
    t.text     "content"
    t.integer  "forum_id"
    t.integer  "user_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.string   "category"
    t.boolean  "legitimate"
    t.integer  "identity_id",  default: 1
    t.index ["city_id"], name: "index_forum_complains_on_city_id", using: :btree
    t.index ["community_id"], name: "index_forum_complains_on_community_id", using: :btree
    t.index ["district_id"], name: "index_forum_complains_on_district_id", using: :btree
    t.index ["forum_id"], name: "index_forum_complains_on_forum_id", using: :btree
    t.index ["identity_id"], name: "index_forum_complains_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_forum_complains_on_local_id", using: :btree
    t.index ["province_id"], name: "index_forum_complains_on_province_id", using: :btree
    t.index ["user_id"], name: "index_forum_complains_on_user_id", using: :btree
  end

  create_table "forums", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.boolean  "published",    default: true
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "identity_id",  default: 1
    t.integer  "score"
    t.index ["city_id"], name: "index_forums_on_city_id", using: :btree
    t.index ["community_id"], name: "index_forums_on_community_id", using: :btree
    t.index ["district_id"], name: "index_forums_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_forums_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_forums_on_local_id", using: :btree
    t.index ["province_id"], name: "index_forums_on_province_id", using: :btree
    t.index ["user_id"], name: "index_forums_on_user_id", using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.string   "lastname"
    t.string   "firstname"
    t.integer  "user_id"
    t.string   "id_number"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "sex",        default: "女"
    t.string   "status",     default: "等待验证"
    t.boolean  "ready",      default: false
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "identity_photos", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "name"
    t.string   "photo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["identity_id"], name: "index_identity_photos_on_identity_id", using: :btree
  end

  create_table "locals", force: :cascade do |t|
    t.string   "name"
    t.string   "pinyin"
    t.integer  "district_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["district_id"], name: "index_locals_on_district_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id",            null: false
    t.integer  "actor_id"
    t.string   "notify_type",        null: false
    t.string   "target_type"
    t.integer  "target_id"
    t.string   "second_target_type"
    t.integer  "second_target_id"
    t.string   "third_target_type"
    t.integer  "third_target_id"
    t.datetime "read_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["user_id", "notify_type"], name: "index_notifications_on_user_id_and_notify_type", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "penalties", force: :cascade do |t|
    t.integer  "penalty_type_id", default: 1
    t.integer  "identity_id"
    t.date     "starting_date"
    t.date     "ending_date"
    t.text     "note"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.index ["city_id"], name: "index_penalties_on_city_id", using: :btree
    t.index ["community_id"], name: "index_penalties_on_community_id", using: :btree
    t.index ["district_id"], name: "index_penalties_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_penalties_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_penalties_on_local_id", using: :btree
    t.index ["penalty_type_id"], name: "index_penalties_on_penalty_type_id", using: :btree
    t.index ["province_id"], name: "index_penalties_on_province_id", using: :btree
    t.index ["user_id"], name: "index_penalties_on_user_id", using: :btree
  end

  create_table "penalty_types", force: :cascade do |t|
    t.string   "name"
    t.text     "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "polls", force: :cascade do |t|
    t.text     "topic"
    t.integer  "user_id"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.boolean  "published",    default: true
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "deadline",     default: 3
    t.index ["city_id"], name: "index_polls_on_city_id", using: :btree
    t.index ["community_id"], name: "index_polls_on_community_id", using: :btree
    t.index ["district_id"], name: "index_polls_on_district_id", using: :btree
    t.index ["local_id"], name: "index_polls_on_local_id", using: :btree
    t.index ["province_id"], name: "index_polls_on_province_id", using: :btree
    t.index ["user_id"], name: "index_polls_on_user_id", using: :btree
  end

  create_table "property_managements", force: :cascade do |t|
    t.string   "name"
    t.string   "telephone"
    t.text     "note"
    t.string   "pinyin"
    t.string   "website"
    t.string   "email"
    t.string   "socialmedia"
    t.integer  "user_id"
    t.boolean  "verified",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["user_id"], name: "index_property_managements_on_user_id", using: :btree
  end

  create_table "provinces", force: :cascade do |t|
    t.string   "name"
    t.string   "pinyin"
    t.string   "pinyin_abbr"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name"], name: "index_provinces_on_name", using: :btree
    t.index ["pinyin"], name: "index_provinces_on_pinyin", using: :btree
    t.index ["pinyin_abbr"], name: "index_provinces_on_pinyin_abbr", using: :btree
  end

  create_table "real_estate_developers", force: :cascade do |t|
    t.string   "name"
    t.string   "pinyin"
    t.string   "telephone"
    t.text     "note"
    t.integer  "city_id"
    t.string   "address"
    t.string   "website"
    t.string   "email"
    t.string   "socialmedia"
    t.integer  "user_id"
    t.boolean  "verified",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["city_id"], name: "index_real_estate_developers_on_city_id", using: :btree
    t.index ["user_id"], name: "index_real_estate_developers_on_user_id", using: :btree
  end

  create_table "report_comment0s", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "report_id"
    t.text     "content"
    t.boolean  "published"
    t.integer  "score"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.integer  "identity_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_report_comment0s_on_city_id", using: :btree
    t.index ["community_id"], name: "index_report_comment0s_on_community_id", using: :btree
    t.index ["district_id"], name: "index_report_comment0s_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_report_comment0s_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_report_comment0s_on_local_id", using: :btree
    t.index ["province_id"], name: "index_report_comment0s_on_province_id", using: :btree
    t.index ["report_id"], name: "index_report_comment0s_on_report_id", using: :btree
    t.index ["user_id"], name: "index_report_comment0s_on_user_id", using: :btree
  end

  create_table "report_comment1s", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "report_comment0_id"
    t.text     "content"
    t.boolean  "published"
    t.integer  "score"
    t.integer  "community_id"
    t.integer  "local_id"
    t.integer  "district_id"
    t.integer  "city_id"
    t.integer  "province_id"
    t.integer  "identity_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["city_id"], name: "index_report_comment1s_on_city_id", using: :btree
    t.index ["community_id"], name: "index_report_comment1s_on_community_id", using: :btree
    t.index ["district_id"], name: "index_report_comment1s_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_report_comment1s_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_report_comment1s_on_local_id", using: :btree
    t.index ["province_id"], name: "index_report_comment1s_on_province_id", using: :btree
    t.index ["report_comment0_id"], name: "index_report_comment1s_on_report_comment0_id", using: :btree
    t.index ["user_id"], name: "index_report_comment1s_on_user_id", using: :btree
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "building"
    t.string   "surround"
    t.string   "floor"
    t.string   "facility_type"
    t.string   "unit"
    t.string   "entity"
    t.string   "representative"
    t.string   "contact_type"
    t.date     "fix_date"
    t.decimal  "attitude"
    t.decimal  "fix_time"
    t.decimal  "fix_result",     default: "0.0"
    t.decimal  "average"
    t.text     "note1"
    t.integer  "identity_id"
    t.boolean  "closed"
    t.text     "note2"
    t.integer  "province_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.integer  "local_id"
    t.integer  "community_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "sex"
    t.boolean  "published",      default: true
    t.index ["city_id"], name: "index_reports_on_city_id", using: :btree
    t.index ["community_id"], name: "index_reports_on_community_id", using: :btree
    t.index ["district_id"], name: "index_reports_on_district_id", using: :btree
    t.index ["identity_id"], name: "index_reports_on_identity_id", using: :btree
    t.index ["local_id"], name: "index_reports_on_local_id", using: :btree
    t.index ["province_id"], name: "index_reports_on_province_id", using: :btree
    t.index ["user_id"], name: "index_reports_on_user_id", using: :btree
  end

  create_table "residence_photos", force: :cascade do |t|
    t.integer  "residence_id"
    t.string   "name"
    t.string   "photo"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["residence_id"], name: "index_residence_photos_on_residence_id", using: :btree
  end

  create_table "residences", force: :cascade do |t|
    t.string   "unit"
    t.integer  "area"
    t.integer  "identity_id"
    t.integer  "community_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "status",       default: "等待验证"
    t.string   "role"
    t.boolean  "ready",        default: false
    t.string   "property_id"
    t.index ["community_id"], name: "index_residences_on_community_id", using: :btree
    t.index ["identity_id"], name: "index_residences_on_identity_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",          null: false
    t.string   "encrypted_password",     default: "",          null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,           null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "logo",                   default: "identicon"
    t.integer  "level",                  default: 15
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "vote_options", force: :cascade do |t|
    t.string   "title"
    t.integer  "poll_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "votes_count", default: 0, null: false
    t.index ["poll_id"], name: "index_vote_options_on_poll_id", using: :btree
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "vote_option_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_votes_on_user_id", using: :btree
    t.index ["vote_option_id", "user_id"], name: "index_votes_on_vote_option_id_and_user_id", unique: true, using: :btree
    t.index ["vote_option_id"], name: "index_votes_on_vote_option_id", using: :btree
  end

  create_table "worktimes", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "beginning"
    t.integer  "year"
    t.integer  "month"
    t.integer  "day"
    t.datetime "ending",       default: '2017-10-21 06:48:06'
    t.integer  "duration",     default: 0
    t.text     "note"
    t.boolean  "suan",         default: false
    t.integer  "province_id"
    t.integer  "city_id"
    t.integer  "district_id"
    t.integer  "local_id"
    t.integer  "community_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["city_id"], name: "index_worktimes_on_city_id", using: :btree
    t.index ["community_id"], name: "index_worktimes_on_community_id", using: :btree
    t.index ["district_id"], name: "index_worktimes_on_district_id", using: :btree
    t.index ["local_id"], name: "index_worktimes_on_local_id", using: :btree
    t.index ["province_id"], name: "index_worktimes_on_province_id", using: :btree
    t.index ["user_id"], name: "index_worktimes_on_user_id", using: :btree
  end

  add_foreign_key "comment0s", "cities"
  add_foreign_key "comment0s", "communities"
  add_foreign_key "comment0s", "districts"
  add_foreign_key "comment0s", "forums"
  add_foreign_key "comment0s", "identities"
  add_foreign_key "comment0s", "locals"
  add_foreign_key "comment0s", "provinces"
  add_foreign_key "comment0s", "users"
  add_foreign_key "comment1s", "cities"
  add_foreign_key "comment1s", "comment0s"
  add_foreign_key "comment1s", "communities"
  add_foreign_key "comment1s", "districts"
  add_foreign_key "comment1s", "identities"
  add_foreign_key "comment1s", "locals"
  add_foreign_key "comment1s", "provinces"
  add_foreign_key "comment1s", "users"
  add_foreign_key "communities", "locals"
  add_foreign_key "communities", "property_managements"
  add_foreign_key "communities", "real_estate_developers"
  add_foreign_key "communities", "users"
  add_foreign_key "favor_forums", "forums"
  add_foreign_key "favor_forums", "users"
  add_foreign_key "favor_reports", "reports"
  add_foreign_key "favor_reports", "users"
  add_foreign_key "forum_complains", "cities"
  add_foreign_key "forum_complains", "communities"
  add_foreign_key "forum_complains", "districts"
  add_foreign_key "forum_complains", "forums"
  add_foreign_key "forum_complains", "identities"
  add_foreign_key "forum_complains", "locals"
  add_foreign_key "forum_complains", "provinces"
  add_foreign_key "forum_complains", "users"
  add_foreign_key "forums", "cities"
  add_foreign_key "forums", "communities"
  add_foreign_key "forums", "districts"
  add_foreign_key "forums", "identities"
  add_foreign_key "forums", "locals"
  add_foreign_key "forums", "provinces"
  add_foreign_key "forums", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "identity_photos", "identities"
  add_foreign_key "locals", "districts"
  add_foreign_key "penalties", "cities"
  add_foreign_key "penalties", "communities"
  add_foreign_key "penalties", "districts"
  add_foreign_key "penalties", "identities"
  add_foreign_key "penalties", "locals"
  add_foreign_key "penalties", "penalty_types"
  add_foreign_key "penalties", "provinces"
  add_foreign_key "penalties", "users"
  add_foreign_key "polls", "cities"
  add_foreign_key "polls", "communities"
  add_foreign_key "polls", "districts"
  add_foreign_key "polls", "locals"
  add_foreign_key "polls", "provinces"
  add_foreign_key "polls", "users"
  add_foreign_key "property_managements", "users"
  add_foreign_key "real_estate_developers", "cities"
  add_foreign_key "real_estate_developers", "users"
  add_foreign_key "report_comment0s", "cities"
  add_foreign_key "report_comment0s", "communities"
  add_foreign_key "report_comment0s", "districts"
  add_foreign_key "report_comment0s", "identities"
  add_foreign_key "report_comment0s", "locals"
  add_foreign_key "report_comment0s", "provinces"
  add_foreign_key "report_comment0s", "reports"
  add_foreign_key "report_comment0s", "users"
  add_foreign_key "report_comment1s", "cities"
  add_foreign_key "report_comment1s", "communities"
  add_foreign_key "report_comment1s", "districts"
  add_foreign_key "report_comment1s", "identities"
  add_foreign_key "report_comment1s", "locals"
  add_foreign_key "report_comment1s", "provinces"
  add_foreign_key "report_comment1s", "report_comment0s"
  add_foreign_key "report_comment1s", "users"
  add_foreign_key "reports", "cities"
  add_foreign_key "reports", "communities"
  add_foreign_key "reports", "districts"
  add_foreign_key "reports", "identities"
  add_foreign_key "reports", "locals"
  add_foreign_key "reports", "provinces"
  add_foreign_key "reports", "users"
  add_foreign_key "residence_photos", "residences"
  add_foreign_key "residences", "communities"
  add_foreign_key "residences", "identities"
  add_foreign_key "vote_options", "polls"
  add_foreign_key "votes", "users"
  add_foreign_key "votes", "vote_options"
  add_foreign_key "worktimes", "cities"
  add_foreign_key "worktimes", "communities"
  add_foreign_key "worktimes", "districts"
  add_foreign_key "worktimes", "locals"
  add_foreign_key "worktimes", "provinces"
  add_foreign_key "worktimes", "users"
end
