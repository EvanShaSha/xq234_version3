class CreateIdentityPhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :identity_photos do |t|
      t.references :identity, foreign_key: true
      t.string :name
      t.string :photo

      t.timestamps
    end
  end
end
