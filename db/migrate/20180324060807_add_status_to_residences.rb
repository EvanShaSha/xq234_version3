class AddStatusToResidences < ActiveRecord::Migration[5.0]
  def change
    add_column :residences, :status, :string, default: '等待验证'
    add_column :residences, :role, :string
  end

end
