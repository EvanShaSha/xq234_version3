class AddScoreToForums < ActiveRecord::Migration[5.0]
  def change
    add_column :forums, :score, :integer
  end
end
