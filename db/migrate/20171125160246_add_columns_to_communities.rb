class AddColumnsToCommunities < ActiveRecord::Migration[5.0]
  def change
    add_reference :communities, :user, foreign_key: true
    add_column :communities, :category, :string, default: '商品住房'
    add_column :communities, :building, :integer
    add_column :communities, :floor, :integer
    add_column :communities, :unit_per_floor, :integer
    add_column :communities, :occupation_rate, :integer
    add_column :communities, :total_household, :integer

    add_column :communities, :introduction, :text
    add_reference :communities, :real_estate_developer, foreign_key: true, default: 1
    add_reference :communities, :property_management, foreign_key: true, default: 1
    add_column :communities, :launched, :boolean, default: false
    add_column :communities, :note, :text

    add_column :communities, :businessfloors, :integer
    add_column :communities, :managementfee, :numeric
    add_column :communities, :undergroundparkingfloors, :integer
    add_column :communities, :openparking, :boolean, default: false

    add_column :communities, :ownerassociation, :boolean, default: false
  end
end
