class CreateFavorForums < ActiveRecord::Migration[5.0]
  def change
    create_table :favor_forums do |t|
      t.references :user, foreign_key: true
      t.references :forum, foreign_key: true
      t.integer :score

      t.timestamps
    end
  end
end
