class CreateWorktimes < ActiveRecord::Migration[5.0]
  def change
    create_table :worktimes do |t|
      t.references :user, foreign_key: true
      t.datetime :beginning
      t.integer :year
      t.integer :month
      t.integer :day
      t.datetime :ending, default: Time.now + 60
      t.integer :duration, default: 0
      t.text :note
      t.boolean :suan, default: false
  
      t.references :province, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.references :local, foreign_key: true
      t.references :community, foreign_key: true

      t.timestamps
    end
  end
end
