class CreateResidencePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :residence_photos do |t|
      t.references :residence, foreign_key: true
      t.string :name
      t.string :photo

      t.timestamps
    end
  end
end
