class CreateReports < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.references :user, foreign_key: true
      t.string :building
      t.string :surround
      t.string :floor
      t.string :facility_type
      t.string :unit
      t.string :entity
      t.string :representative

      t.string :contact_type
      t.date :fix_date
      t.decimal :attitude
      t.decimal :fix_time
      t.decimal :fix_result, default: 0.0
      t.decimal :average
      t.text :note1
      t.references :identity, foreign_key: true
      t.boolean :closed
      t.text :note2
      t.references :province, foreign_key: true
      t.references :city, foreign_key: true
      t.references :district, foreign_key: true
      t.references :local, foreign_key: true
      t.references :community, foreign_key: true

      t.timestamps
    end
  end
end
