class CreateRealEstateDevelopers < ActiveRecord::Migration[5.0]
  def change
    create_table :real_estate_developers do |t|
      t.string :name
      t.string :pinyin
      t.string :telephone
      t.text :note

      t.references :city, foreign_key: true, default: 1

      t.string :address
      t.string :website
      t.string :email
      t.string :socialmedia
      t.references :user, foreign_key: true
      t.boolean :verified, default: false

      t.timestamps
    end
  end
end
