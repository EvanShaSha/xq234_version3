class CreateIdentities < ActiveRecord::Migration[5.0]
  def change
    create_table :identities do |t|
      t.string :lastname
      t.string :firstname
      t.references :user, foreign_key: true

      t.string :id_number

      t.timestamps
    end
  end
end
