class AddReadyToIdentities < ActiveRecord::Migration[5.0]
  def change
    add_column :identities, :ready, :boolean, default: false
  end
end
