class CreateFavorReports < ActiveRecord::Migration[5.0]
  def change
    create_table :favor_reports do |t|
      t.references :user, foreign_key: true
      t.references :report, foreign_key: true
      t.integer :score

      t.timestamps
    end
  end
end
