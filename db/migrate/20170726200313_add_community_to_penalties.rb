class AddCommunityToPenalties < ActiveRecord::Migration[5.0]
  def change
    add_reference :penalties, :community, foreign_key: true
    add_reference :penalties, :local, foreign_key: true
    add_reference :penalties, :district, foreign_key: true
    add_reference :penalties, :city, foreign_key: true
    add_reference :penalties, :province, foreign_key: true
  end
end
