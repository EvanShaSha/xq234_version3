class CreateReportComment1s < ActiveRecord::Migration[5.0]
  def change
    create_table :report_comment1s do |t|
      t.references :user, foreign_key: true
      t.references :report_comment0, foreign_key: true
      t.text :content
      t.boolean :published
      t.integer :score
      t.references :community, foreign_key: true
      t.references :local, foreign_key: true
      t.references :district, foreign_key: true
      t.references :city, foreign_key: true
      t.references :province, foreign_key: true
      t.references :identity, foreign_key: true

      t.timestamps
    end
  end
end
