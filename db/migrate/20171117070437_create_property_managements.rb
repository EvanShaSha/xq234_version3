class CreatePropertyManagements < ActiveRecord::Migration[5.0]
  def change
    create_table :property_managements do |t|
      t.string :name
      t.string :telephone
      t.text :note
      t.string :pinyin
      t.string :website
      t.string :email
      t.string :socialmedia
      t.references :user, foreign_key: true
      t.boolean :verified, default: false

      t.timestamps
    end
  end
end
