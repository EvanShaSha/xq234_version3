class AddSexToReports < ActiveRecord::Migration[5.0]
  def change
    add_column :reports, :sex, :string
    add_column :reports, :published, :boolean, default: true
  end
end
