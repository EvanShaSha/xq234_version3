class CreateForums < ActiveRecord::Migration[5.0]
  def change
    create_table :forums do |t|

      t.text :content
      t.references :user, foreign_key: true
      t.references :community, foreign_key: true
      t.references :local, foreign_key: true
      t.references :district, foreign_key: true
      t.references :city, foreign_key: true
      t.references :province, foreign_key: true
      t.boolean :published, default: true

      t.timestamps
    end
  end
end
