class RemoveApprovedFromForumComplains < ActiveRecord::Migration[5.0]
  def change
    remove_column :forum_complains, :approved, :boolean
  end
end
