class AddReadyToResidences < ActiveRecord::Migration[5.0]
  def change
    add_column :residences, :ready, :boolean, default: false
    add_column :residences, :property_id, :string
  end
end
