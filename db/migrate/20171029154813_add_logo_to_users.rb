class AddLogoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :logo, :string, default: "identicon"
    #add_column :users, :emailmd5, :string
  end

end
