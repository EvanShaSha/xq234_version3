class CreateResidences < ActiveRecord::Migration[5.0]
  def change
    create_table :residences do |t|
      t.string :unit
      t.integer :area
      t.references :identity, foreign_key: true
      t.references :community, foreign_key: true
    

      t.timestamps
    end
  end
end
