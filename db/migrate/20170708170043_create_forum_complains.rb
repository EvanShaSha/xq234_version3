class CreateForumComplains < ActiveRecord::Migration[5.0]
  def change
    create_table :forum_complains do |t|
      t.text :content
      t.references :forum, foreign_key: true
      t.references :user, foreign_key: true
      
      t.boolean :approved, default: false

      t.timestamps
    end
  end
end
