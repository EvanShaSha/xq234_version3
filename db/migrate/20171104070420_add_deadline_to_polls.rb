class AddDeadlineToPolls < ActiveRecord::Migration[5.0]
  def change
    add_column :polls, :deadline, :integer, default: 3
  end
end
