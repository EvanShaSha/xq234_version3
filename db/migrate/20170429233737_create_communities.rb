class CreateCommunities < ActiveRecord::Migration[5.0]
  def change
    create_table :communities do |t|
      t.string :name
      t.string :pinyin
      t.references :local, foreign_key: true
      t.string :address

      t.integer :starting_year

      t.timestamps
    end
  end
end
