class AddLegitimateToForumComplains < ActiveRecord::Migration[5.0]
  def change
    add_column :forum_complains, :legitimate, :boolean
    add_reference :forum_complains, :identity, foreign_key: true, default: '1'
  end
end
