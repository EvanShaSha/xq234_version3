class AddIdentityToForums < ActiveRecord::Migration[5.0]
  def change
    add_reference :forums, :identity, foreign_key: true, default: '1'
  end
end
