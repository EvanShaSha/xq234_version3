class CreatePenalties < ActiveRecord::Migration[5.0]
  def change
    create_table :penalties do |t|
      t.references :penalty_type, foreign_key: true, default: 1
      t.references :identity, foreign_key: true
      t.date :starting_date
      t.date :ending_date
      t.text :note
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
