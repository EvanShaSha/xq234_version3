class AddSexToIdentities < ActiveRecord::Migration[5.0]
  def change
    
    add_column :identities, :sex, :string, default: '女'
    add_column :identities, :status, :string, default: '等待验证'
  end
end
