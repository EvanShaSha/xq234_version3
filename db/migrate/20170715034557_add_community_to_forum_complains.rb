class AddCommunityToForumComplains < ActiveRecord::Migration[5.0]
  def change
    add_reference :forum_complains, :community, foreign_key: true
    add_reference :forum_complains, :local, foreign_key: true
    add_reference :forum_complains, :district, foreign_key: true
    add_reference :forum_complains, :city, foreign_key: true
    add_reference :forum_complains, :province, foreign_key: true
    add_column :forum_complains, :category, :string
  end
end
