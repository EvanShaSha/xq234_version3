# User.destroy_all
10.times do |n|
  #name  = Faker::Name.name
  email = "#{n+1}@xq234.com"
  password = "password"
  User.create!(#name: name
               email: email,
               logo: "identicon",
               password:              password,
               password_confirmation: password,
               confirmed_at: DateTime.now
               )
end
#
# Local.destroy_all
#Local.create!(name: "川音", pinyin: "chuanyin", district_id: "2063")
#＃heroku run rails db:seed　would stop right here...make the following second step.

# Local.create!(name: "高升桥", pinyin: "gaoshenqiao", district_id: "2063")
# Local.create!(name: "李楼", pinyin: "lilou", district_id: "1405")
# Local.create!(name: "南苑", pinyin: "nanyuan", district_id: "748")

# Local.create!(name: "朝天门", pinyin: "chaotianmen", district_id: "2022")
# Local.create!(name: "两路口", pinyin: "lianglukou", district_id: "2022")
#
# #do not know why only first record got create by seed! probably need to get ElasticSearch work before heroku run rails db:seed
#
# #RealEstateDeveloper.destroy_all
#
# RealEstateDeveloper.create!(name: "请选择房地产开发公司", telephone: "0000000000", verified: "TRUE")
# RealEstateDeveloper.create!(name: "大湖房地产开发公司", telephone: "14002332323", verified: "TRUE")
#
#
# #PropertyManagement.destroy_all
#
# PropertyManagement.create!(name: "系统自动填空", telephone: "0000000000", verified: "TRUE")
# PropertyManagement.create!(name: "太阳管理公司", telephone: "14002332323", verified: "TRUE")
# PropertyManagement.create!(name: "高山管理有限公司", telephone: "14002332323", verified: "TRUE")
#
# #Community.destroy_all
#
# Community.create!(name: "海客瀛洲", pinyin: "haikeyingzhou", local_id: "1", address: "四川成都武侯区川音陕西路２号", starting_year: "2005", user_id: "1", category: "商品住房", occupation_rate: "60", introduction: "虚构的三国蜀国小区", launched: true, businessfloors: "3", managementfee: "1.5", undergroundparkingfloors: "2", openparking: false, ownerassociation: false)
#
#
# #Identity.destroy_all
#
# Identity.create!(lastname: "刘", firstname: "备", user_id: "1", id_number: "120101198601134751", sex: "男", status: "已经验证")
# Identity.create!(lastname: "关", firstname: "羽", user_id: "2", id_number: "120101199301134751", sex: "男", status: "已经验证")
# Identity.create!(lastname: "貂", firstname: "蝉", user_id: "3", id_number: "120101199401134751", sex: "女", status: "已经验证")
# # City.create!(name: "请选择城市", pinyin: "aaa")
#
# #Residence.destroy_all
#
# Residence.create!(unit: "B5502", area: "99", identity_id: "1", community_id: "1", status: "已经验证", role: "住家业主")
# Residence.create!(unit: "A1506", area: "66", identity_id: "2", community_id: "1", status: "已经验证", role: "住家业主")
# Residence.create!(unit: "C1701", area: "150", identity_id: "3", community_id: "1", status: "已经验证", role: "住家业主")





# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# users = User.order(:created_at).take(6)
# 50.times do
#   content = Faker::Lorem.sentence(5)
#   users.each { |user| user.microposts.create!(content: content) }
# end

# users = User.find([21, 12, 8, 9, 23, 26, 27])
# 50.times do
#   content = Faker::Lorem.sentence(5)
#   users.each { |user| user.forums.create!(content: content, community_id: '23', local_id: '15', district_id: '2063', city_id: '236', province_id: '23', published: true) }
# end
