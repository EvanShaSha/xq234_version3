Rails.application.routes.draw do


  resources :report_comment1s
  resources :report_comment0s
  resources :favor_forums
  resources :favor_reports
  mount Notifications::Engine => "/notifications"
  resources :comment0s
  resources :comment1s

  resources :residence_photos
  resources :identity_photos

  resources :reports

  resources :property_managements
  resources :real_estate_developers

  resources :worktimes do
    member do
      get 'edit_management'
    end
  end

  resources :activities
  resources :penalties
  resources :penalty_types
  resources :forum_complains

  resources :votes, only: [:create]

  resources :forums
  # resources :forums do
  #   member do
  #     put "like" => "forums#upvote"
  #   end
  # end


  resources :residences do
    member do
      get 'show_self'
      get 'edit_self'
    end
  end

  resources :identities do
    member do
      get 'show_self'
      get 'edit_self'
    end
  end

  # resources :communities
  # resources :polls

  #resources :locals
  resources :locals do
    collection do
      get :autocomplete
    end
  end

  resources :districts

  resources :communities do
    member do
      get 'polls', 'forums', 'forum_complains', 'penalties', 'edit_volunteer'
    end
  end

  resources :polls

  # resources :polls do
  #   # end

  resources :users, only: [:index, :show, :edit, :update]

  authenticated :user do
    root to: 'home#index', as: :authenticated_root
  end

  root to: redirect('/u/sign_in')
  devise_for :users, :path => 'u'

  #devise_for :users
  #get 'up' => ''
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
