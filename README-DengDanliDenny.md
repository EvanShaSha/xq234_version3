https://young-meadow-89797.herokuapp.com/

Development Philosophy and Rule

1. Starting all models from "rails generate scaffold". If every developer starts like this, a new developer will easily understand the structure of our codes, and quickly pick up where we left off. This does not mean you must use its default controller, model, and view. You can modify, omit them from there, and comments on them.

Please do not try to be too smart by developing codes which no other developer would understand! In another word, please develop simple, clean codes.

2. Please comments your codes as details as possible for helping other new developers to understand your codes and can quickly pick up where you left off. We are running this company with many new developers joining us all the time. Losing one particular developer would not affect our company for lasting. 10% of your job assessment would be how you help other co-workers. (Denny did not do a good job of commenting himself!)

3. Please test your codes manually as many times as possible. Also manual test other models which might be affected. Manual test can make sure your navigation route is user friendly.

4. Check security matrix document and think very carefully who has what kind of access to your model.

5.

====================================================================================================================

Application Designing Logic

why community_id must be in the forums table?

1. if user has moved to a different community and his identity remain unchanged, then his old forums in old community stays.

2. if no community_id imprint in forums table, then his old forums in old community will move to new community as he moves.

3. One user has only one primary community where she resides most of her time. It might be possible to forum in a different community from where her reside. Not developing at this moment.

A user with validated identity and residence should be able to browse all forums and polls by each of cities.  Probably not by each of district, local and community due to privacy concern. Other users might be able to guess who lives in which community.

====================================================================================================================

When did schema.rb appear? Why does it only contain users info, but no microposts info?

what is the difference between instant variable @users and local variable users?

why share/ folder doesn't need a controller?

======================================================================================================================

polymorphic associations

too many polymorphic associations and nested routes can be very confused and hard to maintain. Besides, it is inflexible in terms of validation, minimum and maximum for the entry fields.

In the new page of a polymorphic association(richonrails.com), it could enforce validation rules without any error messages. A user would get confused without those error messages. However, a client side validation rules can be also enforced with javascript, and show error messages, might be the solution.

====================================================================================================================

Why do we only open Reports model for owners, not for renter?

Because renters do not have any direct contracts with the property management company in the contracts.

According to an agent, renter pays HOA fee monthly, and it is in the rental agreement.

====================================================================================================================

在最终结果之前，发起者可以删除自己的投诉，但不能删除其他业主的投诉。鉴定者在最终结果之前，可以修改投诉，不能删除投诉。有了最终结果之后，鉴定者在综合了大家的意见以后，给出最后的评分和评语。此投诉一完结，任何用户不可更改或者删除。

====================================================================================================================

If a user has deleted his account, what would happen? Do we even allow that? What does others do in this situation?

====================================================================================================================

https://icons8.com/

You can search, CDC, download, and customized the icons for free for as long as giving credit to the website.

====================================================================================================================

how to shut down a community once it is active and launched?

1. undelete gem?

2. set community.launched to false.


====================================================================================================================

MD5-Avatar-Generator-with-jQuery-Canvas-ninjatar

gravatar is a great solution but we must depended on someone else's server which is located outside of Great Chinese FireWall. Sometimes, the gravatar does not show up due to unknown reasons.

1. Downloaded from jqueryscript.net. It is a client solution for generating consistent abstract avatar on canvas.

Step 1, it reads the text, for example, unique email, then convert it into md3 hash for storing into database.

Step 2, it draws an unique avatar image on canvas based on the md3 hash from database.

2. However, it must loop through all md5 hash for showing unique avatar image for each different user.

3. I think devise gem allows for creating more columns than email column in hidden input field at devise/registrations/new.html.erb, even though I have not tested for this yet.

4. users/edit.html.erb would work in hidden input field for storing md5 hash into database. It is done.

5. how to make each avatar multiple size? Not done.

6. perhaps we need a javascript for or while loop. a week of my sweats.

====================================================================================================================

activeadmin gem

1. In activeadmin, we cannot know who has made what changes to the database if we have multiple admin_users. I don't think public_activity gem might work for activeadmin.

2. must disable downloading data locally.

3. admin_users can delete each others and edit each others. So we need to disable deleting admin_users. must delete admin_users from raw database with SQL

4. must disable deleting records of each model.

5. only allow edit necessary columns.

=======================================================================================================================

Only One Primary Residences Allowed in Version 1.0

one person can be the owner of two separate unit properties in two different communities. However, he can only use one unit property to be his primary residence or at most of his living time.

we could make the uniqueness of combination of identity_id and community_id, enable him to own multiple units property in multiple communities, but which community of his post will go to?

Technically speaking, we can still enable him to choose which community he wants to post to (this might be too confused for some users themselves.). However, we will only consider of making this feature in some further version, instead of version 1.0.

1. what if the owner has sold his unit property here and moved already?

This is very difficult for us to know. We will have a feature to allow owners for paying HOA fee monthly in the APP. However, we still cannot concluded whom does not pay to us in the APP is not a owner, because he can still pay directly to property_management company. And we cannot stop that.

Allow a user to delete his residence info at ANYTIME so that he can enter a new one as soon as he has brought a new unit property in somewhere else. His identity is still with us while he is in between selling, buying and moving.

This has made me wonder if we need to launch a selling and buying unit property referral business as soon as we have dominate position in the market. Real estate agency business is too heavily depended on off-line operation. Think twice before getting too involved.

2. A user cannot delete his identity whether it is "successfully verified" or not. In realty, you can move to anywhere you want. But you cannot change who you are.

3. There is only one residence for primary residence. One person can be the owner of two separate unit properties in two different communities. However, he ONLY can use one unit property to be his primary residence or at most of his living time.

We could make the uniqueness of combination of identity_id and community_id, thus he can own both unit property in multiple communities, but which community of his post will go to?

Technically speaking, we can still enable him to choose which community he wants to post to, but this could cause some confusions to a regular user. However, we will only consider of making this feature in any further version, instead of version 1.0.

4. it is possible that one unit has two co-owners, several other renters or family members, thus unit cannot be unique.

5. A user's identity can be read many times by many different already-verified-users without approving. gem unread might help.

6. why not go to edit page directly from index page?
Because on show page, it will record reading number of each user.

But in reality, I doubt that this can happen very often. If one waiting-user's identity is incorrect, first already-verified-user will dis-approved it. Likewise, if his identity is correct, first already-verified-user will approved it. The management will watch out for one whom has been in "waiting" status for too long. Why so many already-verified-users has read on it without giving a status? If his identity is incorrect, why would he worry about how many reading he has on his incorrect identity?
===========================================================================

public_activity gem

Is redcrumbs(neith very popular nor actively updated), or stream_rails gem (outsource) a better solution than public_activity gem? We do not know so far.

In OS ubuntu 16.04 with Chrome, Chromium browser, the rails server processes twice the read action, instead of one. This is a browser issue. It is not the case with Firefox and Opera browser. This is why public_activity gem records two read actions in its database. No fix for this issue yet.

-------------------------------------------------------------------------------------------------
worktimes model

It can record the working hours of a volunteer, however, it could be abused by any users. It might be useful for 11 elected HOA members.

Keep it for now.

================================================================================


Version 2.0 development and beyond

1. email verification, and password reset.

2. image storage to Cloudinary, which offers 500mg (100 ~ 200 pictures might be good enough for development) for free, might has all the function of mini_magick offers. images stored locally would be deleted by heroku within a few days.

3. system administration on website.

4. look and feel. CSS only for mobile phone this time. No responsive design for PC so far.

5. is integer large enough to cover 100 millions users? Please change to biginteger later.

6. who allowed whom in for identity and residence models?

7. show how many owners in one community.

8. searching function for many models...

9. better graph for vote model.

10. district with the same name belongs to different cities, will fix this error...

=============================================================================================================

* if an chareactor is too long, it would push the top menu logo to right and out of boundary, and to disappear. I probably would not happen in Chinese. But how do we validate that in new form before user submit? I got CSS text-overflow resolved .

* poll new form is regular, change to simple_form...later.

*
------------------------------------------------------------------

Design Rules

1. menu, fixed top menu for signing out or going back to root.
a. fixed dropup menu for home page.

b. fixed bottom menu for show page if others

c. each page has a back-arrow icon on top for going back to previous page
----------
2. icons, use icons8.com for free and provide a link in our site somewhere for giving them the credit.
a. black, bold and filled icons are clickable. red outline icons are just for showing and emphasized

b. allow 5 icons in a row each occupy 25%, even if it is empty for future development.

c. icon's opacity is 0.5 for inactive.
-----------
3. color, use as less color as possible, never use too many colors.
a. only one background-color

b. text color is black.

c. menu icon is black, bold and filled icons

d. red outlined is for showing and emphasized.

c. submit color is grey
----------------
4. pictures.
a. users' self-provided pictures might be too ugly. however, we might romdonly insert some good-looking neighborhood pictures which we have approved in their own picture library. Do not forget giving them the credit!

5. emogj seems enabled automatically.

6. in the future, we might show their own 5 seconds video if their gravatar got clicked.
----------------------------------------------------------------------------------

acts_as_votable

It automatically generate a table called "votes", however, we already have a table called "votes", which is for poll model.

We will try to program on our own.
---------------------------------------------------------------------------
Devise

In development environment, if the host is localhost, there is no error message. However confirmation email does not get send out. It might got blocked by 139.com. gmail blocked it before and giving a message to explain it. sendgrid blocked it without any error messages, however it shows blocked when you login the account in its dashboard.

If it got send out, the confirmation token goes to localhost, instead of localhost:3000.

In heroku, the error says no routing for /u. no email got send out.

Hopefully, devise confirmable will work in our own server. If it won't, we should use REAL ID_number as the user_id, since it is unique for generating avatar.

---------------------------------------------------------------------

Issues

1. 删除？cancel or ok in English for default, how to change them to Chinese?
2. 个人信息只能上传３次，那照片呢？ perhaps, we should see how users behave in real world before we make a decision.
3. user cannot delete her identity once is "已经验证"，what if she really want to delete her identity and never come back? Once her identity or user got deleted, what would happen to all her associated forums, reports, and votes etc?
4. Corruption might happen if we allow deleting negative subjects of forums, reports, and votes after one hour.
5. If a user enter his own CSS or html code into the database, the database will store the CSS code in the table, however it will not show the style. How not to allow the CSS or HTML code in there initially?

--------------------------------------------------------------------------------------------------------

closure_tree gem for nested-comments

Not flexible. It seems easy but difficult to customize it for what we want. It is difficult to associate with other models.

We need to build our comments system as simple as possible so that we can build more complicated features on top of it.

For example, what if there are so many comments on 3rd level, how are we going to paginate them?

Manually, we can program more nested-comments as many levels as we want according to business needs, and paginate all of them.

-------------------------------------------------------------------------------------------------------
will_paginate gem

which only load up one page at a time instead of loading up all records.

#kaminari 1.1.1 is already built-in and existed in rails 5.0.1,0. more downloaded but less updated.

will_paginate should also work for notifications, but never tried.
------------------------------------------------------------------------------------------------------
029-infinite-scrolling

works in rails 4.2, but not in 5.0.1, no error messages

It might be the coffeeScript problem since it requires strict indention. Tried 3 different tutorials, none worked.

--------------------------------------------------------------------------------------------------------------------------

ElasticSearch and searchkick

ElasticSearch 6.8 should work with searchkick 3.1.3 for Ruby 2.3.3

ElasticSearch 7.3 should work with higher version of searchkick than 3.1.3

I could not get ElasticSearch 6.8 starting...and ElasticSearch 5.2.2 and searchkick 3.1.3 could not work with where condition.

will try ransack but do not know the technical differences.

Bonsai ElasticSearch on heroku does not work with searchkick gem. We will not use heroku server eventually.

-------------------------------------------------------------------------------------------------------------------------
development.log files
in log and public/log folder can become a large file. Please clean it on a regular base.

===========================================================================================

client_side_validations gem
---------------------------

did not work without any error messages. It could has something to do with JavaScript. would try without simple_form with a brand new app later.
It is very difficult to figure out what version is compatiable with current version of Ruby and Rails.

It is a important gem since it can reduce a lot of server load, but not very critical.

=====================================================================================================================
